package com.itrui.buyitbackend.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class Order {

    private Integer     orderId         ;

    private Integer   buyer      ;//   '购买人',
    private User        orderBuyer      ; //一对一

    private Integer   send            ;//   '发货人',
    private Business    ordeSend        ; //一对一

    private Date        orderCreatetime ;//   '订单创建时间',
    private String      sendAdd         ;//   '发货地址',
    private String      accpAdd         ;//   '收货地址',
    private String      orderStatus     ;//   '订单状态',
    private Float       payMoney        ;//   '支付金额',
    private String      remake          ;//   '备注',

    private Integer   product        ;//   '订单商品',
    private Product     orderProduct        ; //一对一

}
