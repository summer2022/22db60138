package com.itrui.buyitbackend.service.Impl;

import com.itrui.buyitbackend.mapper.BusinessMapper;
import com.itrui.buyitbackend.pojo.Business;
import com.itrui.buyitbackend.pojo.User;
import com.itrui.buyitbackend.service.BusinessService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class BusinessServiceImpl implements BusinessService {

    @Autowired
    private BusinessMapper businessMapper;

    @Override
    public Business login(Integer account) {
        log.info("service中的"+String.valueOf(account));


        return businessMapper.login(account);
    }

    @Override
    public boolean businessRegister(Business business) {
        return businessMapper.businessRegister(business) > 0;
    }

    @Override
    public Business getBusinessInfo(Integer id) {
        return businessMapper.getBusinessInfo(id);
    }
}
