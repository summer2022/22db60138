

# 买为后端

## 1 介绍

买为HAP后端，基于Java 8，使用SpringBoot整合的SSM框架进行开发的C/S架构、B/S架构项目。

## 2 总体设计

### 2.1 后端项目架构
基于MVC模式的三层架构
![软件架构图](图片/软件架构图.png#pic_center)

- 数据库：数据持久化
- 数据访问层：对数据库进行CRUD基本操作
- 业务逻辑层：对业务逻辑进行封装，组合数据访问层中的基本功能，形成更加复杂的业务逻辑
- 表现成：接收请求，封装数据，调用业务逻辑，响应数据

### 2.2 数据库设计

#### 2.2.1 系统ER图

![软件ER图](图片/软件ER图.png#pic_center)

#### 2.2.2 用户
- 用户表

    | 含义 | 字段名 | 类型  | 长度 |默认值| 属性| 
    |:-------:|:--------:| :-------------:| :-------------:| :-------------:|:-------------:|
    |用户id|user_id|int ||无|主键，自增长|
    |用户名|username|varchar|10|买为用户+当前时间||
    |用户账号|user_account|int ||无|唯一|
    |用户联系方式|user_phone|varchar|15|无|
    |用户邮箱|user_email|varchar|20|无|
    |用户头像|head_photo|varchar|250|无|
    |用户注册时间|user_createtime|datetime||当前时间|
    |用户地址|user_address|varchar|20|无|
    |用户角色|user_role|varchar|20|普通用户|
    |用户等级|user_level|int||0|
    |用户介绍|user_introduce|varchar|200|该用户暂无介绍|
    |用户登录状态|user_status|int||0
    |用户余额|money|decimal(20,2)||0.00

- 用户ER图
    ![Alt](图片/用户ER图.png#pic_center)
  

#### 2.2.3 地址

- 地址表
    | 含义 | 字段名 | 类型  | 长度 |默认值| 属性| 
    |:-------:|:--------:| :-------------:| :-------------:| :-------------:|:-------------:|
    |地址id|address_id|int ||无|主键，自增长|
    |联系人|address_name|varchar|10|无|非空|
    |联系方式|address_phone|varchar|11|无|非空|
    |地址|address_main|varchar|200|无|非空|
    |详细地址|address_detial|200|无||非空|
    |用户账号|user_account|int||无|非空|

- 地址ER图
    ![Alt](图片/地址ER图.png#pic_center)

#### 2.2.4 商家

- 商家表
    | 含义 | 字段名 | 类型  | 长度 |默认值| 属性| 
    |:-------:|:--------:| :-------------:| :-------------:| :-------------:|:-------------:|
    |商家id|business_id|int ||无|主键，自增长|
    |商家名|business_name|varchar|50|无|非空|
    |商家账号|business_account|int||无|唯一非空|
    |商家密码|business_password|varchar|15|无|非空|

- 商家ER图
    ![Alt](图片/商家ER图.png#pic_center)

#### 2.2.5 商品
- 商品表
    | 含义 | 字段名 | 类型  | 长度 |默认值| 属性| 
    |:-------:|:--------:| :-------------:| :-------------:| :-------------:|:-------------:|
    |商品id|pro_id|int ||无|主键，自增长|
    |商品类型|pro_type|varchar|10|无|非空|
    |商品名|pro_name|varchar|50|无|非空|
    |商品价格|price|decimal(20,2)||无|非空|
    |商品库存|stock|int||无|非空|
    |商品主图|pro_main_picture|varchar|200|无
    |商品图|pro_picture|int||无|
    |发布商家id|business|int||无|非空|
    |购买用户id|buyer|int||无|
    |商品发布时间|pro_createtime|datetime||当前发布时间|非空|
    |商品购买时间|pro_buyertime|datetime||无|
    |商品状态|pro_status|varchar|5|在售|非空|
    |商品介绍|introduce|varchar|200|该商品没有任何介绍|

- 商品ER图
    ![Alt](图片/商品ER图.png#pic_center)

#### 2.2.6 商品图片
- 商品图片表
    | 含义 | 字段名 | 类型  | 长度 |默认值| 属性| 
    |:-------:|:--------:| :-------------:| :-------------:| :-------------:|:-------------:|
    |商品图片id|pic_id|int ||无|主键，自增长|
    |商品图片URI|pic_url|varchar|250|无||
    |商品id|pro_pic_id|int ||无|非空|

- 商品图ER图
    ![Alt](图片/商品图ER图.png#pic_center)

#### 2.2.7 订单

- 订单表
    | 含义 | 字段名 | 类型  | 长度 |默认值| 属性| 
    |:-------:|:--------:| :-------------:| :-------------:| :-------------:|:-------------:|
    |订单id|order_id|int ||无|主键，自增长|
    |够买人|order_buyer|int||无|非空|
    |购买时间|order_createtime|datetime||无|
    |发货地址|send_add|varchar|100|无|
    |收货地址|accp_add|varchar|100|无|
    |订单状态|order_status|varchar|5|无|非空
    |支付金额|pay_money|decimal(20,2)||无|
    |订单备注|remake|varchar|250|暂无备注|非空|
    |订单商品|order_product|int||无|非空|
    |发货人|order_send|int||无|非空

- 订单ER图
    ![Alt](图片/订单ER图.png#pic_center)

#### 2.2.8 购物车

- 购物车表
    | 含义 | 字段名 | 类型  | 长度 |默认值| 属性| 
    |:-------:|:--------:| :-------------:| :-------------:| :-------------:|:-------------:|
    |购物车id|car_id|int ||无|主键，自增长|
    |购物车商品|car_proid|int||无|非空|
    |购物车用户|car_userid|int||无|非空|
    |商品加入购物车时间|car_createtime|datetime||无|非空|
    |购物车数量|pro_num|int||无||

- 购物车ER图
    ![Alt](图片/购物车ER图.png#pic_center)


#### 2.2.9 收藏
- 收藏表
    | 含义 | 字段名 | 类型  | 长度 |默认值| 属性| 
    |:-------:|:--------:| :-------------:| :-------------:| :-------------:|:-------------:|
    |收藏id|coliection_id|int ||无|主键，自增长|
    |收藏商品|collection_proid|int||无|非空|
    |收藏用户|collection_userid|int||无|非空|
    |收藏时间|collection_createtime|datetime||无|非空|

- 收藏ER图
    ![Alt](图片/收藏ER图.png#pic_center)


#### 2.2.10 历史消息

- 历史消息表
    | 含义 | 字段名 | 类型  | 长度 |默认值| 属性| 
    |:-------:|:--------:| :-------------:| :-------------:| :-------------:|:-------------:|
    |历史消息id|message_id|int ||无|主键，自增长|
    |发送人|message_send|int||无|非空|
    |目标用户|message_target|int ||无|非空|
    |消息内容|message_content|varchar|250|无|非空|
    |消息发送时间|message_send_time|varchar|50|无|非空
    |消息类型|message_type|int||无|


- 历史消息ER图
    ![Alt](图片/历史消息ER图.png#pic_center)
    
#### 2.2.11 未读消息

- 未读消息表
    | 含义 | 字段名 | 类型  | 长度 |默认值| 属性| 
    |:-------:|:--------:| :-------------:| :-------------:| :-------------:|:-------------:|
    |未读消息id|mr_id|int ||无|主键，自增长|
    |发送人|sender_send|int||无|非空|
    |目标用户|target_target|int ||无|非空|
    |消息内容|content_content|varchar|250|无|非空|
    |消息发送时间|send_time|varchar|50|无|非空
    |消息类型|type|int||无|

- 未读消息ER图
    ![Alt](图片/未读消息ER图.png#pic_center)

#### 2.2.12 黑名单

- 黑名单表
    | 含义 | 字段名 | 类型  | 长度 |默认值| 属性| 
    |:-------:|:--------:| :-------------:| :-------------:| :-------------:|:-------------:|
    |黑名单id|blacklist_id|int ||无|主键，自增长|
    |用户|blacklist_userid|int ||无|非空|
    |黑名单目标|blacklist_target|int||无|非空|
    |黑名单屏蔽类型|blacklist_shield_type|int||无|非空|

- 黑名单ER图
    ![Alt](图片/拉黑ER图.png#pic_center)

#### 2.2.13 视频
- 视频表
    | 含义 | 字段名 | 类型  | 长度 |默认值| 属性| 
    |:-------:|:--------:| :-------------:| :-------------:| :-------------:|:-------------:|
    |视频id|video_id|int ||无|主键，自增长|
    |视频点赞数|like_num|int||0|非空|
    |视频保存数|save_num|int||0|非空|
    |视频评论数|commentn_num||0|非空|
    |视频分享数|share_num||0|非空|
    |喜欢图标|like_src|varchar|100|/common/icons/like1.png|非空|
    |保存图标|save_src|varchar|100|/common/icons/star1.png|非空|
    |视频作者名|video_name|varchar|50|无|非空|
    |视频标题|video_title|varchar|50|无|非空|
    |视频URI|video|varchar|100|无|非空|

- 视频ER图
    ![Alt](图片/视频ER图.png#pic_center)

### 2.3 前后端交互设计

### 前后端交互设计
- rest风格
    * get(select)
    * post(insert)
    * put(update)
    * delete(delete)
- 交互流程结构
  
  ![Alt](图片/前后端交互.png#pic_center)

- 后端Result自定义响应类。
  
    | 属性名 | 类型  |  备注| 
    |:-------:|:--------:| :-------------:|
    |body|Object|后端响应数据|
    |code|Integer|后端响应码|
    |msg|String|后端响应信息|


- 静态类Code，用于后端响应类的后端响应码赋值。

    | 属性名 | 响应类型  |  响应编码  | 
    |:-------:|:--------:| :-------------:|
    |SAVE_OK|添加成功|2011|
    |SAVE_ERR|添加失败|2010|
    |DELETE_OK|删除成功|2021|
    |DELETE_ERR|删除失败|2020|
    |UPDATE_OK|修改成功|2031|
    |UPDATE_ERR|修改失败|2030|
    |GET_OK|查询成功|2040|
    |GET_ERR|查询失败|2041|


### 2.4 后端接口设计
#### 2.4.1 商家模块
##### 商家登录接口
- 函数名：login
- 请求URI：/business/login
- 请求方法：post
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |business|Business|将账号密码封装成business对象|否|

    ```json
    {
        "businessAccount": 123,
        "businessPassword": "123"
    }
    ```

- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|null|
    |code|后端响应码|成功：2041/失败：2040|
    |msg|响应消息|登录成功/登录失败/账号不存在|

    ```json
    {
        "body": null,
        "code": 2041,
        "msg": "登录成功"
    }
    ```

##### 商家注册接口
- 函数名：businessRegister
- 请求URI：/business/register
- 请求方法：post
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |business|Business|非空business对象|否|

    ```json
    {
        "businessName": "测试",
        "businessAccount": 12356,
        "businessPassword": "123"
    }
    ```

- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|null|
    |code|后端响应码|成功：2011/失败：2010|
    |msg|响应消息|注册成功/注册失败/账号重复|

    ```json
    {
        "body": null,
        "code": 2011,
        "msg": "注册成功"
    }
    ```
##### 获得商家信息接口
- 函数名：getBusinessInfo
- 请求URI：/business/
- 请求方法：get
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |id|int|商家id|否|


- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |businessInfo|Business对象|null|


    ```json
    {
        "businessId": 1,
        "businessName": "鱼龙混杂",
        "businessAccount": 123,
        "businessPassword": "123"
    }
    ```
#### 2.4.2 用户模块

##### 用户登录接口
- 函数名：userLogin
- 请求URI：/users/login
- 请求方法：post
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |user|User|将账号密码封装成User对象|否|

    ```json
    {
        "userAccount": 123456,
        "userPassword":"123456"
    }
    ```

- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|User对象|
    |code|后端响应码|成功：2041/失败：2040|
    |msg|响应消息|登录成功/登录失败/账号不存在|

    ```json
    {
        "body": {
        "userId": 1,
        "userName": "哟壹",
        "userAccount": 123456,
        "userPassword": "123456",
        "userPhone": "110",
        "userEmail": "298505@qq.com",
        "headPhoto": "/images/2022.07.05/2.png",
        "userCreatetime": "2099-06-30T08:35:11.000+00:00",
        "userAddress": "***************",
        "userRole": "管理员",
        "userLevel": 100,
        "userIntroduce": null,
        "userStatus": 0,
        "money": 1.0E10
        },
        "code": 2041,
        "msg": "登录成功"
    }
    ```
##### 用户注册接口
- 函数名：userRegister
- 请求URI：/users/register
- 请求方法：post
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |user|User|非空User对象|否|

    ```json
    {
        "userName":"西乡塘大帅哥",  
        "userAccount":1223,
        "userPassword": "111",
        "userPhone":"1008611",
        "userEmail":"1256444@qq.com",  
        "headPhoto":"我超帅，别看了……",
        "userAddress":"广西南宁西乡塘",
        "userRole":"普通用户",
        "userLevel": 0,
        "userIntroduce":"？？？？西乡塘大帅哥还需要介绍吗，自己去岛上打听打听",
        "userStatus":1,  
        "money":10099
    }
    ```

- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|null|
    |code|后端响应码|成功：2011/失败：2010|
    |msg|响应消息|注册成功/注册失败/账号重复|

    ```json
    {
        "body": null,
        "code": 2011,
        "msg": "注册成功"
    }
    ```
##### 用户修改密码接口
- 函数名：updataUserPasswprd
- 请求URI：/users/up
- 请求方法：put
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |user|User|将账号密码封装成User对象|否|

    ```json
    {
        "userAccount":123456789,
        "userPassword":"123456"
    }
    ```

- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|null|
    |code|后端响应码|成功：2031/失败：2030|
    |msg|响应消息|修改成功/修改失败|

    ```json
    {
        "body": null,
        "code": 2031,
        "msg": "修改成功"
    }
    ```
##### 获取全部用户接口
- 函数名：getAllUSer
- 请求URI：/users
- 请求方法：get
- 请求参数：null
  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|User列表|
    |code|后端响应码|成功：2041/失败：2040|
    |msg|响应消息|查询成功/查询失败|

    ```json
    {
        "body": [
        {
            "userId": 1,
            "userName": "哟壹",
            "userAccount": 123456,
            "userPassword": "123456",
            "userPhone": "110",
            "userEmail": "298505@qq.com",
            "headPhoto": "/images/2022.07.05/2.png",
            "userCreatetime": "2033-06-30T08:35:11.000+00:00",
            "userAddress": "*********",
            "userRole": "管理员",
            "userLevel": 100,
            "userIntroduce": null,
            "userStatus": 0,
            "money": 1.0E10
        },
        ······
    ],
    "code": 2041,
    "msg": "查询成功"
    }
    ``` 
##### 通过账号查找用户接口
- 函数名：getUerByAccount
- 请求URI：/account/
- 请求方法：get
- 请求参数：
  
  | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |account|int|用户账号|否|


  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|User对象|
    |code|后端响应码|成功：2041/失败：2040|
    |msg|响应消息|查询成功/查询失败|

    ```json
    {
        "body": {
            "userId": 1,
            "userName": "哟壹",
            "userAccount": 123456,
            "userPassword": "123456",
            "userPhone": "110",
            "userEmail": "298505@qq.com",
            "headPhoto": "/images/2022.07.05/2.png",
            "userCreatetime": "2033-06-30T08:35:11.000+00:00",
            "userAddress": "*********",
            "userRole": "管理员",
            "userLevel": 100,
            "userIntroduce": null,
            "userStatus": 0,
            "money": 1.0E10
        },
        "code": 2041,
        "msg": "查询成功"
    }
    ``` 
##### 通过id查找用户
- 函数名：getUserById
- 请求URI：/id/
- 请求方法：get
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |id|int|用户id|否|


  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|User对象|
    |code|后端响应码|成功：2041/失败：2040|
    |msg|响应消息|查询成功/查询失败|

    ```json
    {
        "body": {
            "userId": 1,
            "userName": "哟壹",
            "userAccount": 123456,
            "userPassword": "123456",
            "userPhone": "110",
            "userEmail": "298505@qq.com",
            "headPhoto": "/images/2022.07.05/2.png",
            "userCreatetime": "2033-06-30T08:35:11.000+00:00",
            "userAddress": "*********",
            "userRole": "管理员",
            "userLevel": 100,
            "userIntroduce": null,
            "userStatus": 0,
            "money": 1.0E10
        },
        "code": 2041,
        "msg": "查询成功"
    }
    ``` 
#### 2.4.3 商品模块
##### 查找全部商品接口
- 函数名：getAllProducts
- 请求URI：/products
- 请求方法：get
- 请求参数：null
  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|Product列表|
    |code|后端响应码|成功：2041/失败：2040|
    |msg|响应消息|查询成功/查询失败|

    ```json
    {
        "body": [
            {
                "proId": 5,
                "proType": "数码产品",
                "proName": "Apple/苹果13Pro 24期免息",
                "price": 7999.0,
                "stock": 158743,
                "proMainPicture": "http://localhost:8080/images/2022.07.05/iPhone120.jpg",
                "proPicture": null,
                "pictures": [
                    {
                        "picId": 9,
                        "picUrl": "http://localhost:8080/images/2022.07.05/iPhone121.jpg",
                        "proId": 5
                    },
                    {
                        "picId": 10,
                        "picUrl": "http://localhost:8080/images/2022.07.05/iPhone122.jpg",
                        "proId": 5
                    },
                    {
                        "picId": 11,
                        "picUrl": "http://localhost:8080/images/2022.07.05/iPhone123.jpg",
                        "proId": 5
                    },
                    {
                        "picId": 12,
                        "picUrl": "http://localhost:8080/images/2022.07.05/iPhone124.jpg",
                        "proId": 5
                    }
                ],
                "business": {
                    "businessId": 13,
                    "businessName": "Apple 官方旗舰店",
                    "businessAccount": 5569,
                    "businessPassword": "123"
                },
                "buyer": null,
                "proCreatetime": "2022-07-05T01:16:05.000+00:00",
                "proBuytime": null,
                "proStatus": "在售",
                "introduce": "当天发货【24期免息】iPhone/苹果13 Pro 5G手机 苹果官方旗舰店国行正品MINI官网直降max24期分期12pro"
            },
        ......
        ],
        "code": 2041,
        "msg": "查询成功"
    }
    ``` 

##### 添加商品接口
- 函数名：addProduct
- 请求URI：/products
- 请求方法：post
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |product|Product对象|非空Product对象|否|

    ```json
    {
        "proType": "数码产品",
        "proName": "Canon M6 Mark2",
        "price": 6599.0,
        "stock": 102351,
        "proMainPicture": "ahfgaghgbaghquibnas4687564.4",
        "proPicture": 3,
         "pictures": [
            {
                "picUrl": "123456ghghd"
            },
            {
                "picUrl": "546784ddfff"
            }
        ],
        "business": {
            "businessId": 1,
            "businessName": "鱼龙混杂",
            "businessAccount": 123,
            "businessPassword": "123"
        },
        "buyer": null,
        "proCreatetime": "2022-06-30T11:37:52.000+00:00",
        "proBuytime": null,
        "proStatus": "在售",
        "introduce": "该商品没有任何介绍"
    }
    ```
  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|null|
    |code|后端响应码|成功：2011/失败：2010|
    |msg|响应消息|添加成功/添加失败|

    ```json
    {
        "body":null,
        "code": 2041,
        "msg": "添加成功"
    }
    ``` 

##### 多条件查询接口
- 函数名：getProductByNameAndTypeAndStatus
- 请求URI：/products/condition
- 请求方法：post
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |product|Product对象|将商品名（选填），商品分类（选填），商品状态（选填）封装成Product对象|可为空|

    ```json
    {
        "proType": "数码产品",
        "proName": "Apple",
        "proStatus": "在售"
    }

    ```
  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|Product列表|
    |code|后端响应码|成功：2041/失败：2040|
    |msg|响应消息|查询成功/查询失败|

    ```json
    {
        "body": [
            {
                "proId": 5,
                "proType": "数码产品",
                "proName": "Apple/苹果13Pro 24期免息",
                "price": 7999.0,
                "stock": 158743,
                "proMainPicture": "http://localhost:8080/images/2022.07.05/iPhone120.jpg",
                "proPicture": null,
                "pictures": [
                    {
                        "picId": 9,
                        "picUrl": "http://localhost:8080/images/2022.07.05/iPhone121.jpg",
                        "proId": 5
                    },
                    {
                        "picId": 10,
                        "picUrl": "http://localhost:8080/images/2022.07.05/iPhone122.jpg",
                        "proId": 5
                    },
                    {
                        "picId": 11,
                        "picUrl": "http://localhost:8080/images/2022.07.05/iPhone123.jpg",
                        "proId": 5
                    },
                    {
                        "picId": 12,
                        "picUrl": "http://localhost:8080/images/2022.07.05/iPhone124.jpg",
                        "proId": 5
                    }
                ],
                "business": {
                    "businessId": 13,
                    "businessName": "Apple 官方旗舰店",
                    "businessAccount": 5569,
                    "businessPassword": "123"
                },
                "buyer": null,
                "proCreatetime": "2022-07-05T01:16:05.000+00:00",
                "proBuytime": null,
                "proStatus": "在售",
                "introduce": "当天发货【24期免息】iPhone/苹果13 Pro 5G手机 苹果官方旗舰店国行正品MINI官网直降max24期分期12pro"
            },
        ......
        ],
        "code": 2041,
        "msg": "查询成功"
    }
    ``` 
##### 通过商品名查询接口
- 函数名：getProductByName
- 请求URI：/products/name/  
- 请求方法：get
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |name|String |商品名（模糊）|否|

  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|Product列表|
    |code|后端响应码|成功：2041/失败：2040|
    |msg|响应消息|查询成功/查询失败|

    ```json
    {
        "body": [
            {
                "proId": 5,
                "proType": "数码产品",
                "proName": "Apple/苹果13Pro 24期免息",
                "price": 7999.0,
                "stock": 158743,
                "proMainPicture": "http://localhost:8080/images/2022.07.05/iPhone120.jpg",
                "proPicture": null,
                "pictures": [
                    {
                        "picId": 9,
                        "picUrl": "http://localhost:8080/images/2022.07.05/iPhone121.jpg",
                        "proId": 5
                    },
                    {
                        "picId": 10,
                        "picUrl": "http://localhost:8080/images/2022.07.05/iPhone122.jpg",
                        "proId": 5
                    },
                    {
                        "picId": 11,
                        "picUrl": "http://localhost:8080/images/2022.07.05/iPhone123.jpg",
                        "proId": 5
                    },
                    {
                        "picId": 12,
                        "picUrl": "http://localhost:8080/images/2022.07.05/iPhone124.jpg",
                        "proId": 5
                    }
                ],
                "business": {
                    "businessId": 13,
                    "businessName": "Apple 官方旗舰店",
                    "businessAccount": 5569,
                    "businessPassword": "123"
                },
                "buyer": null,
                "proCreatetime": "2022-07-05T01:16:05.000+00:00",
                "proBuytime": null,
                "proStatus": "在售",
                "introduce": "当天发货【24期免息】iPhone/苹果13 Pro 5G手机 苹果官方旗舰店国行正品MINI官网直降max24期分期12pro"
            },
        ......
        ],
        "code": 2041,
        "msg": "查询成功"
    }
    ``` 

##### 通过商品类名查询接口
- 函数名：getProductByType
- 请求URI：/products/type/  
- 请求方法：get
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |type|String |商品类名|否|

  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|Product列表|
    |code|后端响应码|成功：2041/失败：2040|
    |msg|响应消息|查询成功/查询失败|

    ```json
    {
        "body": [
            {
                "proId": 5,
                "proType": "数码产品",
                "proName": "Apple/苹果13Pro 24期免息",
                "price": 7999.0,
                "stock": 158743,
                "proMainPicture": "http://localhost:8080/images/2022.07.05/iPhone120.jpg",
                "proPicture": null,
                "pictures": [
                    {
                        "picId": 9,
                        "picUrl": "http://localhost:8080/images/2022.07.05/iPhone121.jpg",
                        "proId": 5
                    },
                    {
                        "picId": 10,
                        "picUrl": "http://localhost:8080/images/2022.07.05/iPhone122.jpg",
                        "proId": 5
                    },
                    {
                        "picId": 11,
                        "picUrl": "http://localhost:8080/images/2022.07.05/iPhone123.jpg",
                        "proId": 5
                    },
                    {
                        "picId": 12,
                        "picUrl": "http://localhost:8080/images/2022.07.05/iPhone124.jpg",
                        "proId": 5
                    }
                ],
                "business": {
                    "businessId": 13,
                    "businessName": "Apple 官方旗舰店",
                    "businessAccount": 5569,
                    "businessPassword": "123"
                },
                "buyer": null,
                "proCreatetime": "2022-07-05T01:16:05.000+00:00",
                "proBuytime": null,
                "proStatus": "在售",
                "introduce": "当天发货【24期免息】iPhone/苹果13 Pro 5G手机 苹果官方旗舰店国行正品MINI官网直降max24期分期12pro"
            },
        ......
        ],
        "code": 2041,
        "msg": "查询成功"
    }
    ``` 

##### 通过商品id查询接口
- 函数名：getProductById
- 请求URI：/products/id/  
- 请求方法：get
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |id|int |商品id|否|

  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|Product列表|
    |code|后端响应码|成功：2041/失败：2040|
    |msg|响应消息|查询成功/查询失败|

    ```json
    {
        "body": [
            {
                "proId": 5,
                "proType": "数码产品",
                "proName": "Apple/苹果13Pro 24期免息",
                "price": 7999.0,
                "stock": 158743,
                "proMainPicture": "http://localhost:8080/images/2022.07.05/iPhone120.jpg",
                "proPicture": null,
                "pictures": [
                    {
                        "picId": 9,
                        "picUrl": "http://localhost:8080/images/2022.07.05/iPhone121.jpg",
                        "proId": 5
                    },
                    {
                        "picId": 10,
                        "picUrl": "http://localhost:8080/images/2022.07.05/iPhone122.jpg",
                        "proId": 5
                    },
                    {
                        "picId": 11,
                        "picUrl": "http://localhost:8080/images/2022.07.05/iPhone123.jpg",
                        "proId": 5
                    },
                    {
                        "picId": 12,
                        "picUrl": "http://localhost:8080/images/2022.07.05/iPhone124.jpg",
                        "proId": 5
                    }
                ],
                "business": {
                    "businessId": 13,
                    "businessName": "Apple 官方旗舰店",
                    "businessAccount": 5569,
                    "businessPassword": "123"
                },
                "buyer": null,
                "proCreatetime": "2022-07-05T01:16:05.000+00:00",
                "proBuytime": null,
                "proStatus": "在售",
                "introduce": "当天发货【24期免息】iPhone/苹果13 Pro 5G手机 苹果官方旗舰店国行正品MINI官网直降max24期分期12pro"
            },
            ......
        ],
        "code": 2041,
        "msg": "查询成功"
    }
    ``` 
##### 通过商品id删除接口
- 函数名：delProductById
- 请求URI：/products/
- 请求方法：delete
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |id|int |商品id|否|

  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|null|
    |code|后端响应码|成功：2021/失败：2020|
    |msg|响应消息|删除成功/删除失败|

    ```json
    {
        "body":null,
        "code": 2021,
        "msg": "删除成功"
    }
    ``` 
##### 修改商品信息接口
- 函数名：updateProduct
- 请求URI：/products
- 请求方法：put
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |product|Product对象 |非空Product对象|否|

   ```json
    {
        "proId": 4,
        "proType": "数码产品",
        "proName": "Sony A7M4",
        "price": 6599.0,
        "stock": 102351,
        "proMainPicture": "ahfgaghgbaghquibnas4687564.4",
        "proPicture": 3,
        "pictures": [
            {
                "picUrl": "123456ghghd"
            },
            {
                "picUrl": "546784ddfff"
            }
        ],
        "proCreatetime": "2022-06-30T11:37:52.000+00:00",
        "proStatus": "在售",
        "introduce": "该商品没有任何介绍"
    }
    ``` 
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|null|
    |code|后端响应码|成功：2031/失败：2030|
    |msg|响应消息|修改成功/修改失败|

    ```json
    {
        "body":null,
        "code": 2031,
        "msg": "修改成功"
    }
    ``` 
#### 2.4.4 订单模块
##### 查找用户订单接口
- 函数名：getAllOrderByUserAccount
- 请求URI：/order/  
- 请求方法：get
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |account|int |用户账号|否|

  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|Order列表|
    |code|后端响应码|成功：2041/失败：2040|
    |msg|响应消息|查询成功/查询失败|

    ```json
    {
        "body": [
            {
                "orderId": 2,
                "buyer": null,
                "orderBuyer": {
                    "userId": null,
                    "userName": null,
                    "userAccount": 1223,
                    "userPassword": "111",
                    "userPhone": "1008611",
                    "userEmail": "1256444@qq.com",
                    "headPhoto": "我超帅，别看了……",
                    "userCreatetime": "2022-07-08T07:20:56.000+00:00",
                    "userAddress": "1223",
                    "userRole": "普通用户",
                    "userLevel": 0,
                    "userIntroduce": "？？？？西乡塘大帅哥还需要介绍吗，自己去岛上打听打听",
                    "userStatus": 1,
                    "money": 10099.0
                },
                "send": null,
                "ordeSend": {
                    "businessId": 1,
                    "businessName": "鱼龙混杂",
                    "businessAccount": 123,
                    "businessPassword": "123"
                },
                "orderCreatetime": "2022-07-05T12:30:16.000+00:00",
                "sendAdd": "美国思雅茹",
                "accpAdd": "日本东西向",
                "orderStatus": "待发货",
                "payMoney": 600.0,
                "remake": "别急买卖啦，送给老婆的，一定要准时送到",
                "product": null,
                "orderProduct": {
                    "proId": 14,
                    "proType": "护肤化妆",
                    "proName": "花西子浮雕口红",
                    "price": 129.0,
                    "stock": 55555,
                    "proMainPicture": "http://localhost:8080/images/2022.07.05/hxz0.jpg",
                    "proPicture": null,
                    "pictures": [
                        {
                            "picId": 48,
                            "picUrl": "http://localhost:8080/images/2022.07.05/hxz4.jpg",
                            "proId": 14
                        },
                        {
                            "picId": 47,
                            "picUrl": "http://localhost:8080/images/2022.07.05/hxz3.jpg",
                            "proId": 14
                        },
                        {
                            "picId": 46,
                            "picUrl": "http://localhost:8080/images/2022.07.05/hxz2.jpg",
                            "proId": 14
                        },
                        {
                            "picId": 45,
                            "picUrl": "http://localhost:8080/images/2022.07.05/hxz1.jpg",
                            "proId": 14
                        }
                    ],
                    "business": null,
                    "buyer": null,
                    "proCreatetime": "2022-07-05T01:35:38.000+00:00",
                    "proBuytime": null,
                    "proStatus": "在售",
                    "introduce": "花西子雕花口红/浮雕唇膏女半哑光正红烂番茄色中国风杜鹃定制款"
                }
            },
            ......
        ],
        "code": 2041,
        "msg": "查询成功"
    }
    ``` 
##### 修改订单状态接口
- 函数名：updateOrderStatusById
- 请求URI：/order
- 请求方法：put
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |order|Order对象 |将订单id和订单状态封装成Order对象|否|

   ```json
    {
        "orderId":2,
        "orderStatus":"待发货"
    }
    ``` 
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|null|
    |code|后端响应码|成功：2031/失败：2030|
    |msg|响应消息|修改成功/修改失败|

    ```json
    {
        "body":null,
        "code": 2031,
        "msg": "修改成功"
    }
    ``` 
##### 通过订单类型查询接口
- 函数名：getAllOrderByType
- 请求URI：/products/ / 
- 请求方法：get
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |account|int |用户账号|否|
    |type|String|订单类型|否|

  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|Order列表|
    |code|后端响应码|成功：2041/失败：2040|
    |msg|响应消息|查询成功/查询失败|

    ```json
    {
        "body": [
            {
                "orderId": 2,
                "buyer": null,
                "orderBuyer": {
                    "userId": null,
                    "userName": null,
                    "userAccount": 1223,
                    "userPassword": "111",
                    "userPhone": "1008611",
                    "userEmail": "1256444@qq.com",
                    "headPhoto": "我超帅，别看了……",
                    "userCreatetime": "2022-07-08T07:20:56.000+00:00",
                    "userAddress": "1223",
                    "userRole": "普通用户",
                    "userLevel": 0,
                    "userIntroduce": "？？？？西乡塘大帅哥还需要介绍吗，自己去岛上打听打听",
                    "userStatus": 1,
                    "money": 10099.0
                },
                "send": null,
                "ordeSend": null,
                "orderCreatetime": "2022-07-05T12:30:16.000+00:00",
                "sendAdd": "美国思雅茹",
                "accpAdd": "日本东西向",
                "orderStatus": "待发货",
                "payMoney": 600.0,
                "remake": "别急买卖啦，送给老婆的，一定要准时送到",
                "product": null,
                "orderProduct": {
                    "proId": 14,
                    "proType": "护肤化妆",
                    "proName": "花西子浮雕口红",
                    "price": 129.0,
                    "stock": 55555,
                    "proMainPicture": "http://localhost:8080/images/2022.07.05/hxz0.jpg",
                    "proPicture": null,
                    "pictures": [
                        {
                            "picId": 48,
                            "picUrl": "http://localhost:8080/images/2022.07.05/hxz4.jpg",
                            "proId": 14
                        },
                        {
                            "picId": 47,
                            "picUrl": "http://localhost:8080/images/2022.07.05/hxz3.jpg",
                            "proId": 14
                        },
                        {
                            "picId": 46,
                            "picUrl": "http://localhost:8080/images/2022.07.05/hxz2.jpg",
                            "proId": 14
                        },
                        {
                            "picId": 45,
                            "picUrl": "http://localhost:8080/images/2022.07.05/hxz1.jpg",
                            "proId": 14
                        }
                    ],
                    "business": null,
                    "buyer": null,
                    "proCreatetime": "2022-07-05T01:35:38.000+00:00",
                    "proBuytime": null,
                    "proStatus": "在售",
                    "introduce": "花西子雕花口红/浮雕唇膏女半哑光正红烂番茄色中国风杜鹃定制款"
                }
            },
            ······
        ],
        "code": 2041,
        "msg": "查询成功"
    }
    ``` 
##### 添加订单接口
- 函数名：addProduct
- 请求URI：/order
- 请求方法：post
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |order|Order对象|非空Order对象|否|

    ```json
    {
        "buyer": 123456,
        "send": 123,
        "sendAdd": "非礼宾十字街",
        "accpAdd": "英国高中路",
        "orderStatus": "待发货",
        "payMoney": 3010.0,
        "remake": "红红火火恍恍惚惚或",
        "product":13
    }
    ```
  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|null|
    |code|后端响应码|成功：2011/失败：2010|
    |msg|响应消息|添加成功/添加失败|

    ```json
    {
        "body":null,
        "code": 2011,
        "msg": "添加成功"
    }
    ``` 
##### 通过订单id删除接口
- 函数名：delOrderById
- 请求URI：/order/
- 请求方法：delete
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |id|int |订单id|否|

  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|null|
    |code|后端响应码|成功：2021/失败：2020|
    |msg|响应消息|删除成功/删除失败|

    ```json
    {
        "body":null,
        "code": 2021,
        "msg": "删除成功"
    }
    ``` 
#### 2.4.5 收藏模块
##### 查找用户收藏接口
- 函数名：getAllCollection
- 请求URI：/collection/  
- 请求方法：get
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |account|int |用户账号|否|

  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|MyCollection列表|
    |code|后端响应码|成功：2041/失败：2040|
    |msg|响应消息|查询成功/查询失败|

    ```json
    {
        "body": [
            {
                "collectionId": 3,
                "collectionCreatetime": "2022-07-06T02:30:53.000+00:00",
                "collectionUserid": null,
                "collectionUser": {
                    "userId": 13,
                    "userName": "西乡塘大帅哥",
                    "userAccount": 1223,
                    "userPassword": "111",
                    "userPhone": "1008611",
                    "userEmail": "1256444@qq.com",
                    "headPhoto": "我超帅，别看了……",
                    "userCreatetime": "2022-07-08T07:20:56.000+00:00",
                    "userAddress": "1223",
                    "userRole": "普通用户",
                    "userLevel": 0,
                    "userIntroduce": "？？？？西乡塘大帅哥还需要介绍吗，自己去岛上打听打听",
                    "userStatus": 1,
                    "money": 10099.0
                },
                "collectionProid": null,
                "collectionPro": {
                    "proId": 14,
                    "proType": "护肤化妆",
                    "proName": "花西子浮雕口红",
                    "price": 129.0,
                    "stock": 55555,
                    "proMainPicture": "http://localhost:8080/images/2022.07.05/hxz0.jpg",
                    "proPicture": null,
                    "pictures": [
                        {
                            "picId": 48,
                            "picUrl": "/images/2022.07.05/hxz4.jpg",
                            "proId": 14
                        },
                        {
                            "picId": 47,
                            "picUrl": "/images/2022.07.05/hxz3.jpg",
                            "proId": 14
                        },
                        {
                            "picId": 46,
                            "picUrl": "/images/2022.07.05/hxz2.jpg",
                            "proId": 14
                        },
                        {
                            "picId": 45,
                            "picUrl": "/images/2022.07.05/hxz1.jpg",
                            "proId": 14
                        }
                    ],
                    "business": null,
                    "buyer": null,
                    "proCreatetime": "2022-07-05T01:35:38.000+00:00",
                    "proBuytime": null,
                    "proStatus": "在售",
                    "introduce": "花西子雕花口红/浮雕唇膏女半哑光正红烂番茄色中国风杜鹃定制款"
                },
                "collectionNum": 8
            },
            ......
        ],
        "code": 2041,
        "msg": "查询成功"
    }
    ``` 

##### 通过收藏id删除接口
- 函数名：delCollection
- 请求URI：/collection/
- 请求方法：delete
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |id|int |收藏id|否|

  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|null|
    |code|后端响应码|成功：2021/失败：2020|
    |msg|响应消息|删除成功/删除失败|

    ```json
    {
        "body":null,
        "code": 2021,
        "msg": "删除成功"
    }
    ``` 
##### 添加收藏接口
- 函数名：addCollection
- 请求URI：/Collection
- 请求方法：put
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |MyCollection|MyCollection对象|非空MyCollection对象|否|

    ```json
    {
        "collectionUserid": 123465,
        "collectionProid": 8
    }
    ```
  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|null|
    |code|后端响应码|成功：2041/失败：2040|
    |msg|响应消息|添加成功/添加失败|

    ```json
    {
        "body":null,
        "code": 2041,
        "msg": "添加成功"
    }
    ``` 
#### 2.4.6 购物车模块
##### 查找用户购物车接口
- 函数名：getAllCar
- 请求URI：/car/  
- 请求方法：get
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |account|int |用户账号|否|

  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|Car列表|
    |code|后端响应码|成功：2041/失败：2040|
    |msg|响应消息|查询成功/查询失败|

    ```json
    {
        "body": [
            {
                "carId": 1,
                "carProid": null,
                "carPro": {
                    "proId": 13,
                    "proType": "数码产品",
                    "proName": "aoc 24G2 小金刚",
                    "price": 1099.0,
                    "stock": 156983,
                    "proMainPicture": "http://localhost:8080/images/2022.07.05/show0.jpg",
                    "proPicture": null,
                    "pictures": null,
                    "business": null,
                    "buyer": null,
                    "proCreatetime": "2022-07-05T01:33:26.000+00:00",
                    "proBuytime": null,
                    "proStatus": "在售",
                    "introduce": "AOC 24G2小金刚144Hz显示器24英寸IPS电竞1ms响应电脑液晶屏27G2"
                },
                "carUserid": null,
                "carUser": {
                    "userId": 1,
                    "userName": "哟壹",
                    "userAccount": 123456,
                    "userPassword": "123456",
                    "userPhone": "110",
                    "userEmail": "298505@qq.com",
                    "headPhoto": "/images/2022.07.05/2.png",
                    "userCreatetime": "2022-06-30T08:35:11.000+00:00",
                    "userAddress": "桂林电子科技大学花江校区",
                    "userRole": "管理员",
                    "userLevel": 100,
                    "userIntroduce": null,
                    "userStatus": 0,
                    "money": 1.0E10
                },
                "carCreatetime": "2022-07-06T03:13:37.000+00:00",
                "proNum": 1,
                "proBusinessName": "鱼龙混杂"
            },
            ......
        ],
        "code": 2041,
        "msg": "查询成功"
    }
    ``` 

##### 通过购物车id删除接口
- 函数名：delCar
- 请求URI：/car/
- 请求方法：delete
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |id|int |收藏id|否|

  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|null|
    |code|后端响应码|成功：2021/失败：2020|
    |msg|响应消息|删除成功/删除失败|

    ```json
    {
        "body":null,
        "code": 2021,
        "msg": "删除成功"
    }
    ``` 
##### 添加购物车接口
- 函数名：addCar
- 请求URI：/car
- 请求方法：post
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |car|Car对象|非空Car对象|否|

    ```json

    {
        "carProid": 6,
        "carUserid": 123456,
        "proNum": 1
    }
    ```
  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|null|
    |code|后端响应码|成功：2011/失败：2010|
    |msg|响应消息|添加成功/添加失败|

    ```json
    {
        "body":null,
        "code": 2011,
        "msg": "添加成功"
    }
    ``` 
#### 2.4.7 我的地址模块
##### 查找用户地址接口
- 函数名：getAllAddressByAccount
- 请求URI：/address/  
- 请求方法：get
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |account|int |用户账号|否|

  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|MyAddress列表|
    |code|后端响应码|成功：2041/失败：2040|
    |msg|响应消息|查询成功/查询失败|

    ```json
    {
        "body": [
            {
                "addressId": 1,
                "addressName": "欢瑞",
                "addressPhone": "11111111111",
                "addressMain": "广西******县",
                "userAccount": 123456,
                "addressDetail": "****************"
            },
            ......
        ],
        "code": 2041,
        "msg": "查询成功"
    }
    ``` 

##### 通过地址id删除接口
- 函数名：delAddressById
- 请求URI：/address/
- 请求方法：delete
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |id|int |地址id|否|

  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|null|
    |code|后端响应码|成功：2021/失败：2020|
    |msg|响应消息|删除成功/删除失败|

    ```json
    {
        "body":null,
        "code": 2021,
        "msg": "删除成功"
    }
    ``` 
##### 修改我的地址接口
- 函数名：updateAddress
- 请求URI：/address
- 请求方法：put
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |myAddress|MyAddress对象|非空MyAddress对象|否|

    ```json

    {
        "addressName": "狗蛋",
        "addressPhone": "1669903620",
        "addressMain": "********************",
        "userAccount": 123456,
        "addressDetail": "******************"
    }
    ```
  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|null|
    |code|后端响应码|成功：2031/失败：2030|
    |msg|响应消息|修改成功/修改失败|

    ```json
    {
        "body":null,
        "code": 2031,
        "msg": "修改成功"
    }
    ``` 
##### 添加我的地址接口
- 函数名：addAddress
- 请求URI：/address
- 请求方法：post
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |myAddress|MyAddress对象|非空MyAddress对象|否|

    ```json

    {
        "addressName": "狗蛋",
        "addressPhone": "1669903620",
        "addressMain": "********************",
        "userAccount": 123456,
        "addressDetail": "******************"
    }
    ```
  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|null|
    |code|后端响应码|成功：2011/失败：2010|
    |msg|响应消息|添加成功/添加失败|

    ```json
    {
        "body":null,
        "code": 2011,
        "msg": "添加成功"
    }
    ``` 
#### 2.4.8 消息模块
##### 获取用户未读消息数量接口
- 函数名：getUnreadCount
- 请求URI：/unread/ / 
- 请求方法：get
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |userid|int |当前用户id|否|
    |target|int|目标用户id|否|

  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    ||int|用户未读数量消息|

##### 添加黑名单接口
- 函数名：addBlackList
- 请求URI：/unread/black/ / / 
- 请求方法：get
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |userid|int |当前用户id|否|
    |target|int|目标用户id|否|
    |type|int|屏蔽类型|否|

  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    ||boolean|是否已经添加黑名单|


##### 清除未读消息接口
- 函数名：delMessage
- 请求URI：/unread/delm/ / 
- 请求方法：get
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |userid|int |当前用户id|否|
    |target|int|目标用户id|否|

  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    ||boolean|是否已经删除消息|

##### 清除历史消息接口
- 函数名：delALlMessage
- 请求URI：/unread/dela/ / 
- 请求方法：get
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |userid|int |当前用户id|否|
    |target|int|目标用户id|否|

  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    ||boolean|是否已经删除消息|


##### 检查用户是否存在黑名单接口
- 函数名：isBlack
- 请求URI：/unread/isblack/ / /
- 请求方法：get
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |userid|int |当前用户id|否|
    |target|int|目标用户id|否|
    |type|int|屏蔽类型|否|
  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    ||boolean|是否用户是否存在黑名单|

##### 解除黑名单接口
- 函数名：isBlack
- 请求URI：/unread/delblack/ / /
- 请求方法：get
- 请求参数：
  
    | 参数 | 类型  |  说明  | 是否为空|
    |:-------:|:--------:| :-------------:| :-------------:|
    |userid|int |当前用户id|否|
    |target|int|目标用户id|否|
    |type|int|屏蔽类型|否|
  
- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    ||boolean|用户是否解除黑名单|
#### 2.4.9 视频模块
##### 查找全部视频接口
- 函数名：getALlVideos
- 请求URI：/video
- 请求方法：get
- 请求参数：null

- 响应示例：
  
    | 参数名 | 参数说明 | 备注 |
    |:-------:|:--------:| :-------------:|
    |body|返回数据|Car列表|
    |code|后端响应码|成功：2041/失败：2040|
    |msg|响应消息|查询成功/查询失败|

    ```json
    {
        "body": [
            {
                "videoId": 1,
                "likeNum": 1235,
                "saveNum": 1235,
                "commentNum": null,
                "shareNum": 125,
                "likeSrc": "/common/icons/like1.png",
                "saveSrc": null,
                "name": "@西乡塘大帅哥",
                "title": "超帅打篮球，抢篮板到国人上篮一条龙，我的男生#篮球#帅哥#校园#",
                "video": "http://localhost:8080/images/videos/videos0.mp4"
            },
            ......
        ],
        "code": 2041,
        "msg": "查询成功"
    }
    ``` 


