

# 任务2 环境搭建(虚拟机)

## 前言介绍

本篇介绍搭建的编译环境win10+Vmware虚拟机Ubuntu系统+docker，本篇尽量以通俗易懂的方式讲解。

## 下载虚拟机VMware Workstation Player/PRO

下载链接：[点击](https://customerconnect.vmware.com/en/downloads/details?downloadGroup=WKST-PLAYER-1623-NEW&productId=1039&rPId=85399)选择window版本

![image-20220715183338190](环境搭建（虚拟机）.assets/image-20220715183338190.png)

# 在虚拟机下安装Ubuntu系统(用于拉取、编译源代码)

## 下载Ubuntu镜像

在虚拟机下安装Ubuntu系统首先需要下载Ubuntu20.4的系统iso镜像文件 [下载](https://mirrors.tuna.tsinghua.edu.cn/ubuntu-releases/20.04/ubuntu-20.04.4-desktop-amd64.iso)

## 在虚拟机上安装Ubuntu

下载完成后打开Vmware虚拟机，点击左上角  **文件—新建虚拟机** 

<img src="环境搭建（虚拟机）.assets/image-20220715185610126.png" alt="image-20220715185610126" style="zoom:67%;" />

下一步直到此界面，在此界面下选择下载好的Ubuntu Iso镜像，用于安装Ubuntu系统。

<img src="环境搭建（虚拟机）.assets/image-20220715184737539.png" alt="image-20220715184737539" style="zoom:67%;" />

将虚拟机安装路径与存储移至C盘以外，以确保有足够的空间拉取源码

<img src="环境搭建（虚拟机）.assets/image-20220715185027265.png" alt="image-20220715185027265" style="zoom:67%;" />

虚拟机空间设置建议**80G-120G**，设置将**虚拟硬盘拆分成多个文件**

<img src="环境搭建（虚拟机）.assets/image-20220715185722498.png" alt="image-20220715185722498" style="zoom:67%;" />

## 完成Ubuntu系统安装

选择英文输入法，英文语言**（防止因为中文目录而产生后续编译报错）**

<img src="环境搭建（虚拟机）.assets/image-20220715190103608.png" alt="image-20220715190103608" style="zoom:67%;" />

下一步直至Install now，选择continue，等待加载

<img src="环境搭建（虚拟机）.assets/image-20220715190410794.png" alt="image-20220715190410794" style="zoom:67%;" />

根据系统提示完成步骤，等待系统安装………………

## 配置Ubuntu

- 更换国内软件源（方便下载各种包）
- 切换中文语言（可选）

### 更换国内软件源

打开右下角菜单——搜索**Software & update**

<img src="环境搭建（虚拟机）.assets/image-20220715192251992.png" alt="image-20220715192251992" style="zoom:67%;" />

下载源选择Other——China——阿里源/清华源tsinghua

<img src="环境搭建（虚拟机）.assets/image-20220715192607533.png" alt="image-20220715192607533" style="zoom:67%;" />

<img src="环境搭建（虚拟机）.assets/image-20220715192738896.png" alt="image-20220715192738896" style="zoom: 80%;" />

### 设置中文语言（不会产生编译错误）

在菜单打开**setting**——**region &  language**

<img src="环境搭建（虚拟机）.assets/image-20220715193200489.png" alt="image-20220715193200489" style="zoom:67%;" />

# 安装Docker容器

关于Docker介绍：[点击]([(44条消息) Docker基础：Docker是什么，为什么这么火_IT技术分享社区的博客-CSDN博客](https://blog.csdn.net/xishining/article/details/125772787)) 

打开终端：右键桌面——Open in Terminal  或者 CTRL + ALT +T

输入指令更新软件列表

```bash
sudo apt-get update 
```

安装相关软件工具

```bash
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
```

设置Docker官方密钥

```bash
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
```



设置Docker仓库**（下面代码块是一条命令，请全部复制输入终端）**

```bash
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

安装Docker引擎

```bash
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```

运行Docker容器Helloworld测试

```bash
sudo docker run hello-world
```

# 获取Docker容器镜像并启动

## 下载镜像官方编译镜像

执行一下命令获取官方编译镜像

```bash
sudo docker pull swr.cn-south-1.myhuaweicloud.com/openharmony-docker/openharmony-docker:1.0.0
```

## 查看已有的Docker镜像

```bash
sudo docker images
```

返回信息如下：

```bash
REPOSITORY                                                               TAG       IMAGE ID       CREATED        SIZE
swr.cn-south-1.myhuaweicloud.com/openharmony-docker/openharmony-docker   1.0.0     31f50584dab1   4 months ago   4.34GB
hello-world                                                              latest    feb5d9fea6a5   9 months ago   13.3kB

```

## 设置Docker容器

映射本地文件夹与Docker容器文件夹（共享文件夹）

指令需更改的信息如下：

- 用户名 

- 用于启动镜像的容器名

- 本地映射的文件夹目录

  ```bash
  sudo docker run --net=host -v /home/用户名/openharmony:/home/openharmony/ --name 容器名 -it 31f50584dab1   /bin/bash
  ```

例如： 用户名：sixstar  |  本地文件夹路径/home/sixstar/openharmony映射至虚拟机/home/openharmony   |  容器名称devenv

```bash
sudo docker run --net=host -v /home/sixstar/openharmony:/home/openharmony/ --name devenv -it 31f50584dab1 /bin/bash
```

执行后将进入容器

## 如何在退出容器后重新进入

退出容器：**CTRL + D ** 后面再运行可以直接运行并连接我们之前创建的容器

```bash
sudo docker start 目标容器ID/名字
```

```bash
sudo docker exec -it 目标容器ID/名字 /bin/bash
```

例：

```
sudo docker start devenv
sudo docker exec -it devenv /bin/bash
```

也可以停止容器：

```
sudo docker stop 目标容器ID
```

# 更新容器编译环境

编译前：进入容器后输入以下命令

```bash
apt-get install lib32stdc++6
```

```bash
dpkg --add-architecture i386
```

```shell
apt-get update
```

```bash
apt-get upgrade
```

# 拉取源码与编译源码均在Docker容器下执行（推荐）

[指导链接]([docs/UnionpiTiger_helloworld/获取源码.md · OpenHarmony-SIG/knowledge_demo_temp - 码云 - 开源中国 (gitee.com)](https://gitee.com/openharmony-sig/knowledge_demo_temp/blob/master/docs/UnionpiTiger_helloworld/获取源码.md))

# 如遇git获取失败请参考一下

[配置与使用Git](https://gitee.com/openharmony-sig/knowledge/blob/master/docs/openharmony_getstarted/push_pr/readme.md)

