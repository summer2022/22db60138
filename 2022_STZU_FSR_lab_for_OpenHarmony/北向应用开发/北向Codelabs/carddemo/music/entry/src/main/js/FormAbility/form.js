import formBindingData from '@ohos.application.formBindingData';
import formInfo from '@ohos.application.formInfo';
import formProvider from '@ohos.application.formProvider';
var obj={
    "pic_num":"1"
}
var formData=formBindingData.createFormBindingData(obj);
export default {
    onCreate(want) {
        // Called to return a FormBindingData object.
        // 获取卡片ID
        let formId = want.parameters["ohos.extra.param.key.form_identity"];
        obj.pic_num="1";
        formData=formBindingData.createFormBindingData(obj);
        //设置一个计时器，循环播放图片形成动画。
        //从第一张图片开始，这里的动图分解后只有10帧，那么我们的上限就是10
        let i=1;
        setInterval(()=>{
            //如果已经播放到第10帧了，那么切换回第一帧，实现循环播放
            if(i>30){
                i=1;
            }
            //修改pic_num的值，递增播放
            obj.pic_num=i.toString();
            //绑定数据
            formData = formBindingData.createFormBindingData(obj);
            //更新卡片
            formProvider.updateForm(formId,formData).catch((err)=>{
               console.info("yzj"+JSON.stringify(err));
                            })
            //播放下一帧
            i++;
        //每帧持续90ms，这里是根据实际情况设定的
        },100);
        return formData;
    },
    onCastToNormal(formId) {

        // Called when the form provider is notified that a temporary form is successfully
        // converted to a normal form.

        console.info("yzj: onCastToNormal");
    },

    onUpdate(formId) {
        // Called to notify the form provider to update a specified form.
    },

    onVisibilityChange(newStatus) {
        // Called when the form provider receives form events from the system.
    },

    onEvent(formId, message) {
        // Called when a specified message event defined by the form provider is triggered.
    },

    onDestroy(formId) {
        // Called to notify the form provider that a specified form has been destroyed.
    },

    onAcquireFormState(want) {
        // Called to return a {@link FormState} object.
        return formInfo.FormState.READY;
    }
}