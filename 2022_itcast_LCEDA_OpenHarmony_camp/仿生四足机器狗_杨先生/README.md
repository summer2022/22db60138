### 背景：

偶然看到了一个训练营活动：

![截图.png](media/876d9f042a8041bfddb0b8f35a5b6783.png)

正巧我有嵌入式开发的基础，也想学习下机械设计和PCB设计，提高自己的综合技能。

于是报了名，开始实践做一只基于鸿蒙系统的仿生四足机器狗。

### 外形设计：

这里使用的是Solidworks，跟着训练营的老师学习了基本的绘图和3D建模。

为了节省成本，设计为可使用亚克力拼装的外壳。

绘制设计图纸：

![截图.png](media/ae74de2c5a021f322b6e6b638f7a6e40.png)

### 3D建模：

涉及到版权问题，这里就放一个官方的预览图了，我也是按照这个模型来设计的。

![截图.png](media/0e9d78a2e9c9578e465e2acb9ed2bc8f.png)

设计好外观的同时机器狗的零部件也可以确定下来，四条腿采用8个舵机驱动，前脸装雷达，顶部为以后升级预留孔位。

### 元器件选型：

主控采用传智HI3861鸿蒙IOTWiFi开发模组，这个是训练营指定的模组，不能改。

![截图.png](media/af8cdcb938c45a33058907b01e6df713.png)

控制芯片采用PCA9685，这个芯片可以提供16路PWM，目前只用到8路，多出的引脚引出便于以后升级。

![CBE96728CDA7E3ED806F3A7D8DAA8836.jpg](media/679abb55576a71d216a62b3c027e2079.jpeg)

舵机采用MG90S，金属齿轮180度，价格便宜性能稳定。

![截图.png](media/199a5e3547e564daf634afcf45fe5e3a.png)

电源由于芯片是3.3V的，舵机是5V的，因此我采用7.4V锂电池，降压到5V，然后降压到3.3V分别给芯片和舵机供电。

7.4V 电池

![截图.png](media/7101b3c3725072b187690bb469a2031f.png)

LDO线性稳压器(7.4V转5V)

LM-1084S-5.0

C259973(立创商城编号)

![截图.png](media/630895a7535a9dad9602d1e4cab7d2c6.png)

AMS1117-3.3 (5V转3V3)

C347222(立创商城编号)

![截图.png](media/ddb2098e8512bc9e305b82cdc1bb62b0.png)

### 电路原理图设计：

![截图.png](media/a29ea05fd967ac0c16e2278e9f873fad.png)

使用国产免费的立创EDA进行设计，这里要有电子电路基础的知识，要会看各种元器件的规格书和基本电路图，为此我也是复习了一遍高中物理电学部分。

### PCB电路设计：

![截图.png](media/370bee57659e5f1dd2067b046e15e4bb.png)

使用立创EDA画好原理图之后便可以转PCB，这里要规划一下各个元器件摆放的位置，基本为了方便布线来摆放的。同时需要遵循一些基本设计规则：GND铺铜、天线下方镂空、走线尽量贴紧走钝角，电源线加粗等。这里我也是第一次布线，有点乱哈。

PCB板预览：

![截图.png](media/f1be616d6cd6c79d6eef0611631c3f99.png)

PCB效果图：

![截图.png](media/abba06e4d4f0fbde47cb1f7bb832e325.png)

### 购买材料：

PCB板画好之后可以直接在嘉立创下单免费打样。

外壳导出DXF格式工程图给淘宝的亚克力板厂家切割制作，花费20元。

bom配件单直接在立创商城下单，参加训练营完成PCB设计领了80元的元器件券，自己再另外花了20元的运费，想省钱的话排针可以只买1x20P的拆开用，我为了方便全买了。

![截图.png](media/2200bd402ac45a6570f446d1a92f48a3.png)

舵机在淘宝买的：

![截图.png](media/e66c304cdfb75d8dbe8d47ce40bb509f.png)

焊接用工具也必不可少，我之前买了烙铁，再买了一个拆焊台和锡膏方便芯片和贴片元件的焊接：

![截图.png](media/0e406052a0537abb922ad73bbe0f5ff5.png)

PCA9685立创没货，也是淘宝买的，加上电池、紧固件等总共花费200元左右。

### 开始组装：

![截图.png](media/ca79ae482be171b1cf9f136d2d94b0b1.png)

亚克力很好组装，按照之前设计图做的3D装配体来装就行，分分钟搞定。

焊接对我来说比较难，因为没什么经验，因此我还专门买了块焊接练习板先练练手：

![截图.png](media/5d95c24ed1406ff83d6ce65d398418f7.png)

练习中：

![截图.png](media/68e1ca267e156001be3fed7f5b643d2e.png)

焊接成功：

![截图.png](media/e1fd96194a2db70b750e1d4647cb9eae.png)

开始实战：

![截图.png](media/60a1cc1a1edeb1f0ebe2a8e6d16ef776.png)

放上元器件，涂上锡膏，开始加热、观察，如果有移位的用镊子摆正即可：

![截图.png](media/ccf4c037fc358264323eca6d127742ed.png)

焊接好了，再焊上排针:

![截图.png](media/e8b6281695649816777c40209b045384.png)

成了，接下来观察有没有连锡和虚焊，再用万用表测量确认。

发现PCA这个引脚有连锡：

![截图.png](media/a80e5f378299747e20bf1dd968b52faf.png)

涂点锡膏，再用烙铁和吸锡带处理一下：

![截图.png](media/96d4bb675555ec34f33ee74eb9eb3824.png)

确认没问题之后上电，用TTL转USB连接电脑测试：

![截图.png](media/0bdfdbc75b3b857230b050024227bed4.png)

能连接成功，烧录程序也成功，程序我用的是训练营的测试固件：

![截图.png](media/c05f9a8ffba3415b0b070dc1c3c441a2.png)

但是i2c通讯的时候报错了：

![截图.png](media/5c832d0cb32cec26bc1bf3a6d0d0d9ac.png)

用万用表测试了，找了很久没找到原因，求助了群友，最后还是立创的老师发现了问题：

![截图.png](media/b857cf8202f6be31abad3dc696dd8617.png)

原来是我画原理图的时候马虎了，把SCL的上拉电阻给下拉了。

老师还贴心的给出了解决方案：

![截图.png](media/ea5adfc685fb007b748c80f2812c72cd.png)

于是乎开始改造电路：

![截图.png](media/7b5cf28b813209439fdc9a56b5ceb514.png)

这个电阻宽度只有0.8mm，用镊子夹着手抖一下就飞了，废了半天劲终于把这个电阻搭上去了。

把舵机都连上，测试都动了起来，一切正常，激动ing：

![截图.png](media/189cd778064ccb25cff412e96e04bc25.png)

开始组装机器狗与电路板：

![截图.png](media/39bdaf996b010404e754ca9573eb6bcc.png)

为了方便调试，顶板还没有装，效果如图：

![截图.png](media/36a2dd3cb4d73ed1ef78b85647158410.png)

连上WIFI，就可以远程控制了：

这里通过控制两个舵机的角度来控制狗腿的运动还要用到运动学知识

![截图.png](media/f987d936675be4a0d030fefbaafdfc6d.png)

机器人控制WEB界面(没截图，用的群友的图)：

![截图.png](media/40ca526cb8bcb64438f44548c4002139.png)

### 动态图：

![8c06b8a474fc23f70bed48302ec664ba20227211428482.gif](media/c3ae6ca09e5b9e95d98baec61ed06e50.gif)

到此机器狗1.0版本算是完成了，接下来就是软件方面的调试优化和不断的升级迭代了。

开发板的所有接口都引出了，后续添加模块很方便。

后续打算给机器狗加上各种传感器，摄像头，屏幕，喇叭，尾巴，灯光等等，让机器狗更智能更酷炫。

项目已上传到立创开源平台：

https://oshwhub.com/specher/sp-ji-qi-mao

### 结语：

历时一个月的训练营，我收获颇多。

每天晚上下班回家之后就打开B站看老师们的教学直播，跟着老师动手实践从0到1完成了这只机器狗。

期间系统学习了Solid Works，可以制作一些简单的模型了。

学习了从立创EDA平台原理图设计到PCB布局，布线，打样。

了解了运动学，电路基础知识，焊接知识，鸿蒙系统开发环境等。

感谢立创平台提供的这次学习机会，不仅不收学费，而且还发放元器件券。

感谢传智教育老师的答疑解惑。

---
### 2.0版本机器猫：

在原来基础上增加了type-c接口和串口模块，可以直接USB连接电脑烧录程序。

并增加了开关，LED电源灯，改进了reset按钮电路，优化了PCB走线。

原理图：

![Schematic_【训练营】sp机器猫 2.0_2022-08-04.png](media/34432b2e2ac62f45f93ef9f286c12eaf.png)

3D预览实物图：

![截图.png](media/0875c1fc23ef00746561fcdfd3de2aaf.png)

所有元器件和板子到货，开始焊接。

焊接TYPE-C花了半天时间，焊坏了三个板子三个头，要么连锡，要么焊好了插上去一下就给拔下来。

最后发现是我用的低温锡膏的锅，焊接插拔件一定要使用焊锡丝，不要用低温锡膏。这也算是踩坑了积累经验了。

最终成果图：

![6d9f3a8786923a8eb65f62c10b1a64652022841638531.gif](media/822e6bb0004ad6798ace084f17ce57ec.gif)

后续加模块升级将继续跟进。


### 参考资料:

SolidWorks教程 <http://heimarobot.cn/blog/29>

用于开源鸿蒙仿生狗教学源码 <https://gitee.com/genkipi/ohosdog>

仿生机器狗训练营＞固件开发环境 <https://www.bilibili.com/video/BV1Jg411X7sZ>

传智鸿蒙元气派开发环境搭建 <https://robot.czxy.com/ohos/day03/vmware%26ubuntu.html>

保姆级教程：从0到1带你做稚晖君HoloCubic小电视 <https://www.bilibili.com/video/BV11h41147iJ>

【焊接教学】技小新手把手教你焊接元器件 <https://www.bilibili.com/video/BV1eJ411K7rM>

四足机器人——步态规划 <https://blog.csdn.net/m0_58585940/article/details/121445129>

Moco-8四足机器人控制算法简介 <https://zhuanlan.zhihu.com/p/69869440>

〈软件目录〉▷22年6月Win版 <https://mp.weixin.qq.com/s/OyHq_rR53vLrfef8GTbo5A>


