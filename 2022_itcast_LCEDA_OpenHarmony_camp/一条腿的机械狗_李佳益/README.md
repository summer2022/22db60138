# 立创EDA+传智教育的HI3861鸿蒙模组打造一条腿的狗 心得

#### 介绍

​		偶然在网上看到了立创EDA搞得训练营活动，突然就想挑战一下自己。一是自己的年龄比较大，二来也没有相关的经验。如果能做出来，回事一件很酷的事情。就跟着立创EDA一步一步的做了起来。

​		其实一条腿的狗，就是本次训练营中基础版，是一个可以写字的小贱种。

#### 心得

1.画板子用立创eda，纯中文界面对小白很友好，但是在覆铜和更换元器件后更新问题，困扰了一阵。最终还是画了出来。

![pcb](F:\contest\2022_itcast_LCEDA_OpenHarmony_camp\mrrdt\img\pcb.png)

别说实物还挺好看的。

![微信图片_20220816200540](F:\contest\2022_itcast_LCEDA_OpenHarmony_camp\mrrdt\img\微信图片_20220816200540.jpg)

2.亚克力切割找的淘宝，9.9包邮。笔架很容易破损补发了一次。

3.焊板子可能是对小白来说最大的挑战了。多方尝试，最终买了个小焊台。其他元件还好，PCA9685引脚过于密，我铺铜还是45°角，PCA9685的焊盘有锯齿，感觉焊的不理想。以后还是外接PCA9685模块吧。

4.固件对于没有C语言基础的，就只能照猫画虎了。

![Uy03Foui1GsXQi3jJqCi3SmkMAZ28fvSiiCLD5kG](F:\contest\2022_itcast_LCEDA_OpenHarmony_camp\mrrdt\img\Uy03Foui1GsXQi3jJqCi3SmkMAZ28fvSiiCLD5kG.jpeg)

5.本次活动最大的心得就是，动手给自己一个挑战，当一个问题困扰自己很久后，突然解决了，给自己带来的成就感。



#### 小贱钟开源在这里：https://oshwhub.com/itheima/ohosdog_copy 

#### 相关资料百度网盘：https://pan.baidu.com/s/1OztwrWfjke83tJw3oP9gRQ?pwd=qu2s 

#### 完整讲解视频：https://space.bilibili.com/430536057?spm_id_from=333.337.0.0 

