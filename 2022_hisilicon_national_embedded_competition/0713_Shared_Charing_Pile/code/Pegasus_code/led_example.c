/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"

#include "plate.h"
#include "flag.h"

#define LED_INTERVAL_TIME_US 100000
#define LED_TASK_STACK_SIZE 10240
#define LED_TASK_PRIO 25
#define LED_TEST_GPIO 9 // for hispark_pegasus

//static long long g_iState = 0;

//enum LedState {
//    LED_ON = 0,
//    LED_OFF,
//    LED_SPARK,
//};

//enum LedState g_ledState = LED_SPARK;



static void *LedTask(const char *arg)
{
    (void)arg;
    printf("flag111:%d\n",flag);

    while (1) {
        
        IoTGpioSetOutputVal(LED_TEST_GPIO, !flag);
        
        if(gpio_flag[0]){IoTGpioSetOutputVal(GPIO[0], 1);}
        else{IoTGpioSetOutputVal(GPIO[0], 0);}
        if(gpio_flag[1]){IoTGpioSetOutputVal(GPIO[1], 1);}
        else{IoTGpioSetOutputVal(GPIO[1], 0);}
        if(gpio_flag[2]){IoTGpioSetOutputVal(GPIO[2], 1);}
        else{IoTGpioSetOutputVal(GPIO[2], 0);}
        if(gpio_flag[3]){IoTGpioSetOutputVal(GPIO[3], 1);}
        else{IoTGpioSetOutputVal(GPIO[3], 0);}
        if(gpio_flag[4]){IoTGpioSetOutputVal(GPIO[4], 1);}
        else{IoTGpioSetOutputVal(GPIO[4], 0);}
        if(gpio_flag[5]){IoTGpioSetOutputVal(GPIO[5], 1);}
        else{IoTGpioSetOutputVal(GPIO[5], 0);}
        
        //IoTGpioSetOutputVal(5, 0);
        //IoTGpioSetOutputVal(7, 1);
        //IoTGpioSetOutputVal(8, 1);
        
        usleep(LED_INTERVAL_TIME_US);
        /*
        IoTGpioSetOutputVal(LED_TEST_GPIO, flag);
       
        if(gpio_flag[0]){IoTGpioSetOutputVal(GPIO[0], 0);}
        else{IoTGpioSetOutputVal(GPIO[0], 1);}
        if(gpio_flag[1]){IoTGpioSetOutputVal(GPIO[1], 0);}
        else{IoTGpioSetOutputVal(GPIO[1], 1);}
        if(gpio_flag[2]){IoTGpioSetOutputVal(GPIO[2], 0);}
        else{IoTGpioSetOutputVal(GPIO[2], 1);}
        if(gpio_flag[3]){IoTGpioSetOutputVal(GPIO[3], 0);}
        else{IoTGpioSetOutputVal(GPIO[3], 1);}
        if(gpio_flag[4]){IoTGpioSetOutputVal(GPIO[4], 0);}
        else{IoTGpioSetOutputVal(GPIO[4], 1);}
        if(gpio_flag[5]){IoTGpioSetOutputVal(GPIO[5], 0);}
        else{IoTGpioSetOutputVal(GPIO[5], 1);}
        */
        //IoTGpioSetOutputVal(5, 1);
        //IoTGpioSetOutputVal(7, 0);
        //IoTGpioSetOutputVal(8, 0);
        
        printf("flag222:%d\n",flag);
        printf("VLP:%s\n",vlp1.plate);
        printf("gpio1:%d\n",gpio_flag[0]);
        printf("gpio2:%d\n",gpio_flag[1]);
        printf("gpio3:%d\n",gpio_flag[2]);
        printf("gpio4:%d\n",gpio_flag[3]);
        printf("gpio5:%d\n",gpio_flag[4]);
        printf("gpio6:%d\n",gpio_flag[5]);
        //printf("gpio4:%d\n",gpio_flag[3]);
        /*
        switch (g_ledState) {
           case LED_ON:
                IoTGpioSetOutputVal(LED_TEST_GPIO, 1);
                usleep(LED_INTERVAL_TIME_US);
                g_iState++;
                break;
            case LED_OFF:
                IoTGpioSetOutputVal(LED_TEST_GPIO, 0);
                usleep(LED_INTERVAL_TIME_US);
                g_iState++;
                break;
            case LED_SPARK:
                IoTGpioSetOutputVal(LED_TEST_GPIO, 0);
                usleep(LED_INTERVAL_TIME_US);
                IoTGpioSetOutputVal(LED_TEST_GPIO, 1);
                usleep(LED_INTERVAL_TIME_US);
                g_iState++;
                break;
            default:
                usleep(LED_INTERVAL_TIME_US);
                break;
        }
        if (g_iState == 0xffffffff) {
            g_iState = 0;
            break;
        }
        */
    }
    return NULL;
}

static void LedExampleEntry(void)
{
    osThreadAttr_t attr;

    IoTGpioInit(LED_TEST_GPIO);
    IoTGpioSetDir(LED_TEST_GPIO, IOT_GPIO_DIR_OUT);
    IoTGpioInit(GPIO[0]);  // gpio10
    IoSetFunc(GPIO[0], 0); // gpio10使用0功能(GPIO) 
    IoTGpioSetDir(GPIO[0], IOT_GPIO_DIR_OUT);
    IoTGpioInit(GPIO[1]);  // gpio5
    IoSetFunc(GPIO[1], 0); // gpio5使用0功能(GPIO) 
    IoTGpioSetDir(GPIO[1], IOT_GPIO_DIR_OUT);
    IoTGpioInit(GPIO[2]);  // gpio11
    IoTGpioSetDir(GPIO[2], IOT_GPIO_DIR_OUT);
    IoTGpioInit(GPIO[3]);  // gpio12
    IoTGpioSetDir(GPIO[3], IOT_GPIO_DIR_OUT);
    IoTGpioInit(GPIO[4]);  // gpio7
    IoSetFunc(GPIO[4], 0);       // gpio7使用0功能(GPIO) 
    IoTGpioSetDir(GPIO[4], IOT_GPIO_DIR_OUT);
    IoTGpioInit(GPIO[5]);  // gpio8
    IoSetFunc(GPIO[5], 0);       // gpio8使用0功能(GPIO) 
    IoTGpioSetDir(GPIO[5], IOT_GPIO_DIR_OUT);

    //IoTGpioInit(5);
    //IoSetFunc(5, 0); /* gpio5使用0功能(GPIO) */
    //IoTGpioSetDir(5, IOT_GPIO_DIR_OUT);
    //IoTGpioInit(7);
    //IoSetFunc(7, 0); /* gpio7使用0功能(GPIO) */
    //IoTGpioSetDir(7, IOT_GPIO_DIR_OUT);
    //IoTGpioInit(8);
    //IoSetFunc(8, 0); /* gpio8使用0功能(GPIO) */
    //IoTGpioSetDir(8, IOT_GPIO_DIR_OUT);
    
    attr.name = "LedTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = LED_TASK_STACK_SIZE;
    attr.priority = LED_TASK_PRIO;
    

    if (osThreadNew((osThreadFunc_t)LedTask, NULL, &attr) == NULL) {
        printf("[LedExample] Falied to create LedTask!\n");
    }

}

SYS_RUN(LedExampleEntry);
