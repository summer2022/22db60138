# change_0153

#### 介绍
0555主要是海思嵌入式大赛相关代码，

Taurus目录下，是人脸口罩识别及健康码分类的相关AI插件代码补丁，以及一个训练好的口罩识别分类的模型。

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明


1.  Taurus中包含相关代码及wk模型

		    two_classify.c为健康码分类相关代码；
		
			cnn_trash_classify.c为口罩分类相关代码；
			
			res_19_inst.wk为口罩识别分类网的wk文件；

			res_2_inst.wk为健康码分类网的wk文件；

			0555.pdf.md为技术文档


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
