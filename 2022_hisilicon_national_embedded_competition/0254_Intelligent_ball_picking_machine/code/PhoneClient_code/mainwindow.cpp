#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
   ui->setupUi(this);

   ui->show_state_label->setPixmap(QPixmap(":/disconnect.png").scaled(50,50));   //未连接图片
   //设置默认ip和端口
   ui->ip->setText("192.168.1.1");
   ui->port->setText("5678");

   ui->left_dutyCycle->setText("L97");
   ui->right_dutyCycle->setText("R99");

   m_tcp=new QTcpSocket(this);   //实例化一个tcp对象

   setWindowIcon(QIcon(":/logo.ico"));

   ui->disconnect_button->setDisabled(true);    //开始的时候断开连接按钮不可以用

   //检测断开
   connect(m_tcp,&QTcpSocket::disconnected,this,[=](){
        //服务器主动断开连接
        m_tcp->close();
        m_tcp->deleteLater();
        ui->show_state_label->setPixmap(QPixmap(":/disconnect.png").scaled(50,50));
        ui->disconnect_button->setDisabled(true);
        ui->connect_button->setEnabled(true);
   });
   //检测连接成功
   connect(m_tcp,&QTcpSocket::connected,this,[=](){
         ui->disconnect_button->setEnabled(true);
         ui->connect_button->setDisabled(true);
         ui->show_state_label->setPixmap(QPixmap(":/connected.png").scaled(50,50));   //连接成功图片
   });

   //检测接收的数据
   connect(m_tcp,&QTcpSocket::readyRead,this,[=](){
       QByteArray data=m_tcp->readAll();
       //data保存着server发送来的数据
   });
}


MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_connect_button_clicked()
{
    //连接按钮
    unsigned short port=ui->port->text().toUShort();   //将port输入框中的信息转化为无符号短整型
    QString ip=ui->ip->text();
    m_tcp->connectToHost(QHostAddress(ip),port);        //连接服务器
}


void MainWindow::on_disconnect_button_clicked()
{
    //断开连接按钮
    ui->disconnect_button->setDisabled(true);
    ui->connect_button->setEnabled(true);  //连接按钮生效
    ui->show_state_label->setPixmap(QPixmap(":/disconnect.png").scaled(50,50));   //未连接图片
    m_tcp->disconnectFromHost();    //客户端主动断开连接
    m_tcp->close();
}



void MainWindow::on_forward_btn_clicked()
{
     m_tcp->write("forward");
}


void MainWindow::on_left_btn_clicked()
{
    m_tcp->write("left");
}


void MainWindow::on_back_btn_clicked()
{
     m_tcp->write("back");
}



void MainWindow::on_right_btn_clicked()
{
     m_tcp->write("right");
}


void MainWindow::on_stop_button_clicked()
{
    m_tcp->write("stop");  //控制小车停止运行
}


void MainWindow::on_send_parameters_btn_clicked()
{
    //发送左轮和右轮的占空比
    const char* left_dutyCycle=ui->left_dutyCycle->text().toStdString().c_str();
    m_tcp->write(left_dutyCycle);
}


void MainWindow::on_send_parameters_btn_2_clicked()
{
     const char* right_dutyCycle=ui->right_dutyCycle->text().toStdString().c_str();
     m_tcp->write(right_dutyCycle);
}


void MainWindow::on_start_button_clicked()
{
    m_tcp->write("start");
}


void MainWindow::on_pushButton_2_clicked()
{
    // 手动模式
    m_tcp->write("manual");
    ui->left_btn->setDisabled(false);
    ui->right_btn->setDisabled(false);
    ui->forward_btn->setDisabled(false);
    ui->back_btn->setDisabled(false);
    ui->send_parameters_btn->setDisabled(false);
    ui->send_parameters_btn_2->setDisabled(false);
}


void MainWindow::on_pushButton_clicked()
{
    // auto
    m_tcp->write("auto");
    ui->left_btn->setDisabled(true);
    ui->right_btn->setDisabled(true);
    ui->forward_btn->setDisabled(true);
    ui->back_btn->setDisabled(true);
    ui->send_parameters_btn->setDisabled(true);
    ui->send_parameters_btn_2->setDisabled(true);
}

