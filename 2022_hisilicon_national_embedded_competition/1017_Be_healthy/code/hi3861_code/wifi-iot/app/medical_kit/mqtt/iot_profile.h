/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef IOT_PROFILE_H
#define IOT_PROFILE_H

#include <hi_types_base.h>
#include "iot_config.h"

#define OC_BEEP_STATUS_ON       ((hi_u8) 0x01)
#define OC_BEEP_STATUS_OFF      ((hi_u8) 0x00)

// < enum all the data type for the oc profile
typedef enum {
    EN_IOT_DATATYPE_INT = 0,
    EN_IOT_DATATYPE_LONG,
    EN_IOT_DATATYPE_FLOAT,
    EN_IOT_DATATYPE_DOUBLE,
    EN_IOT_DATATYPE_STRING,           // < must be ended with '\0'
    EN_IOT_DATATYPE_LAST,
}IoTDataType_t;

typedef enum {
    OC_LED_ON = 1,
    OC_LED_OFF
}OcLedValue;

typedef struct {
    void                            *nxt; // < ponit to the next key
    const char                      *key;
    const char                      *value;
    hi_u32                          iValue;
    IoTDataType_t                   type;
}IoTProfileKV_t;

typedef struct {
    void *nxt;
    char *serviceID;
    char *eventTime;
    IoTProfileKV_t *serviceProperty;
}IoTProfileService_t;

typedef struct {
    int  retCode;           // < response code, 0 success while others failed
    const char   *respName; // < response name
    const char   *requestID; // < specified by the message command
    IoTProfileKV_t  *paras; // < the command paras
}IoTCmdResp_t;

typedef struct {
    const char *subState;
    const char *subReport;
    const char *reportVersion;
    const char *Token;
}WeChatProfileStatus;

typedef struct {
    int  ID;                   //Drug ID
    char  Name[50];            //Name
    int  NameLen;               //Length of name
    int  Fill;                //Enabled or not               
    int  DrugTotal;            //Total number of drugs
    int  Location;
    const char *drugid;
    const char *drugname;
    const char *drugNamelen;
    const char *drugfill;
    const char *drugtotal;
    const char *drugloc;
}WeChatProfileDrugsReporte;

typedef struct {
    int  ID;                 //由于时间有限，mqtt长度限制，以一个服药者及一种药物作为演示
    char Name[50];           
    int  NameLen;             
    //int  Fill;                      
    int  DrugNum;           
    int  DrugID;
    char  DrugName[50];
    int  Times;               
    int  Timeout;
    int  DosageTime;
    const char *userid;              
    const char *username;
    const char *userNamelen;
    //const char *userfill;
    const char *userdrugnum;
    const char *userdrugid;            
    const char *userdrugname;
    const char *usertimes;
    const char *usertimeout;
    const char *userdosagetime;
}WeChatProfileUsersReporte;
typedef struct {
    int  ID;                 //由于时间有限，mqtt长度限制，以一个服药者及一种药物作为演示
    char Name[50];                                                 
    char DrugName[50];
    int  Num;           //已服药次数          
    int  Times;         //需服药次数
    char  Hour[5];          //服用时间h
    char  Min[3];          //服用时间m
    const char *userid;              
    const char *username;
    const char *drugname;
    const char *times;            
    const char *num;
    const char *hour;
    const char *min;
}WeChatProfileLogsReporte;
typedef struct {
    const char *subscribeType;
    WeChatProfileStatus status;
    WeChatProfileDrugsReporte reportAction;
}WeChatDrugsProfile;
typedef struct {
    const char *subscribeType;
    WeChatProfileStatus status;
    WeChatProfileUsersReporte reportAction;
}WeChatUsersProfile;
typedef struct {
    const char *subscribeType;
    WeChatProfileStatus status;
    WeChatProfileLogsReporte reportAction;
}WeChatLogsProfile;
/**
 * use this function to report the property to the iot platform
*/
int IoTProfilePropertyDrugsReport(char *deviceID, WeChatDrugsProfile *payload);
int IoTProfilePropertyUsersReport(char *deviceID, WeChatUsersProfile *payload);
int IoTProfilePropertyLogsReport(char *deviceID, WeChatLogsProfile *payload);
void cJsonInit(void);
void WifiStaReadyWait(void);
#endif