#ifndef MEDICAL_KIT_H
#define MEDICAL_KIT_H

#define MOTOR1_GPIO_OUT 2          //Default low level     
#define MOTOR2_GPIO_OUT 8          //Default low level
#define WS2812_LED_GPIO_OUT 9        
#define IR1_GPIO_IN 7                    
#define IR2_GPIO_IN 10 

#define DOOR_OPEN_GPIO_IN   13                  
#define DOOR_CLOSE_GPIO_IN  14

#define VOICE_DATA_LEN  5
#define GPIO_FUNC  0

#define TASK_DELAY 3

#define Users_Num_MAX 10
#define Drug_Num_MAX  10

typedef struct PeopleInf{
   int  ID;                    //People ID
   char Name[100];             //Name
   int  NameLen;               //Length of name
   int  Fill;                 //Enabled or not               
   int  DrugNum;               //Number of medication types taken
   int  DrugID[10];            //Drug ID
   int  Times[10];             //Number of doses per day
   int  TimeOut[10];           //Alarm time(min)
   int  DosageTime[10][10];    //Drug ID:Dosing time(0~23h)
} PeopleInf;

typedef struct DrugInf{
   int  ID;                   //Drug ID
   char Name[100];            //Name
   int  NameLen;               //Length of name
   int  Fill;                //Enabled or not               
   int  DrugTotal;            //Total number of drugs
   int  Location;
} DrugInf;

typedef struct LogInf{
   int Hour;            //Time
   int Min;                 
   int Num[10];            //Number of doses
   int FinishState[10];  //单个药物完成情况 0无需服药 1需服药且已完成 2需服药但未完成 
   int FinishNum;       //完成服药的药物数量
   int UnFinishNum;       //完成服药的药物数量
} LogInf;

typedef struct TimeInf{ 
   int Hour;            //Time
   int Min;                 
   int year;
   int month;
   int day;
} TimeInf;
#endif
/** @} */
