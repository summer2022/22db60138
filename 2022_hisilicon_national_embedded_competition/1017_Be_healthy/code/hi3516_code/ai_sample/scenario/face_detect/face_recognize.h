#ifndef FACE_RECOGNIZE_H
#define FACE_RECOGNIZE_H

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include "hi_comm_video.h"

#if __cplusplus
extern "C" {
#endif

/* load face detect model */
HI_S32 MobileFaceRecLoad(uintptr_t* model);

/* unload face detect model */
HI_S32 MobileFaceRecUnload(uintptr_t model);

/* face detect calculation */
HI_S32 FaceRecCal(const IVE_IMAGE_S *img, float *feature_buff);

HI_S32 FaceRecInit(void);
HI_S32 FaceRecExit(void);

HI_S32 FaceRecCompare(float feature_buff[], float feature_to_compare[], float tolerance,float *dis);

#ifdef __cplusplus
}
#endif
#endif
