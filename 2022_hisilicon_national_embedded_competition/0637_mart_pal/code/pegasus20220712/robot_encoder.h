#ifndef ROBOT_ENCODER_H
#define ROBOT_ENCODER_H

#include <stdio.h>
#include <stdlib.h>

/* PID结构体 */

typedef struct{
    float SetSpeed;                // 设定速度值
    float ActualSpeed;             // 实际速度值
    float err;                     // 当前速度偏差值e_k
    float err_last;                // 上一个偏差值e_k-1
    float integral;                // 积分值
    int voltage;                   // 返回电压值（pwm占空比） 
}MotorPID;

/* 脉冲数换算速度 */
float pwmduty2speed(int duty);

/* 编码器线程入口 */
void encoderTaskInit(void);

/* PID控制 */
void PID_control_right(float setspeed, float actualspeed);
void PID_control_left(float setspeed, float actualspeed);
void PID_control_handler();

/*设置电机速度(百分比)*/
void setLeftMotorDuty(int);
void setRightMotorDuty(int);

/*设置电机速度*/
void setLeftMotorSpeed(float);
void setRightMotorSpeed(float);

#endif