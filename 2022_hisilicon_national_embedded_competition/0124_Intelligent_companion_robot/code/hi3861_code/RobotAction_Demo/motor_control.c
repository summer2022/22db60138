#include "motor_control.h"
int timer_flag = 0;
void delay_ms(unsigned int time){
    hi_udelay(time*1000);
}

static void *Low_frePwmTimerFunc(unsigned int port)
{
    timer_flag = !timer_flag;
    IoTGpioSetOutputVal(port, timer_flag);
}


void Low_frePwmInit(unsigned int port,unsigned short duty, unsigned int freq)
{

    IoTGpioInit(port);
    IoSetFunc(port, 0); /* 设置IO6的功能 */
    IoTGpioSetDir(port, IOT_GPIO_DIR_OUT);

    osTimerAttr_t timerNew;
    timerNew.name = "Low_frePwmTimer";
    timerNew.cb_size = 0U;
    timerNew.attr_bits = 0U;
    timerNew.cb_mem = NULL;
    osTimerId_t Low_frePwmTimerID = osTimerNew((osTimerFunc_t)Low_frePwmTimerFunc,osTimerPeriodic,port,&timerNew);
    if (Low_frePwmTimerID == NULL) {
        printf("[Low_frePwmTimer] Falied to create Task!\n");
    }

    osTimerStart(Low_frePwmTimerID,1000000);
}


/*
duty : 占空比输入1-99
freq：频率HZ
*/
unsigned int period= 0;//ms
unsigned int high = 0;
unsigned int low = 0;
void Low_frePwmStart(unsigned int port, unsigned short duty, unsigned int freq)
{
    if(freq==50)
        period = 19000;//us
    else if(freq == 200)
        period = 4780;//us

    high = period*duty*0.01;
    low = period - high;

    IoTGpioSetOutputVal(port, 1);
    hi_udelay(high);
    IoTGpioSetOutputVal(port, 0);
    hi_udelay(low);
    
}

