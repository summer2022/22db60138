#ifndef DPC_H
#define DPC_H

#include <iostream>
#include "sample_comm_nnie.h"

#if __cplusplus
extern "C" {
#endif

// typedef struct tagIPC_IMAGE {
//     HI_U64 u64PhyAddr;
//     HI_U64 u64VirAddr;
//     HI_U32 u32Width;
//     HI_U32 u32Height;
// } IPC_IMAGE;

class dpc {
public:
    HI_S32 DPCLoad(uintptr_t* model);
    HI_S32 DPCUnload(uintptr_t model);
    // /* DPC calculation */
    HI_S32 DPCCal(uintptr_t model, VIDEO_FRAME_INFO_S *srcFrm, VIDEO_FRAME_INFO_S *dstFrm);
};

#ifdef __cplusplus
}
#endif
#endif