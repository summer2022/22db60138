<!--
 * @Author: error: git config user.name && git config user.email & please set dead value or install git
 * @Date: 2022-08-13 11:09:25
 * @LastEditors: error: git config user.name && git config user.email & please set dead value or install git
 * @LastEditTime: 2022-08-14 07:31:37
 * @FilePath: \undefinedd:\desktop1\contest\2022-hisilicon-national-embedded-competition\智能助老服务系统\README.md
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
-->
# 【项目名称】：智能助老服务系统
# 【负责人】：谢纪浩

## 方案实施流程
#### 【项目描述】：
##### 随着社会的发展，人口老龄化问题日趋严重的背景，助老服务机器人应运而生。本项目即设计一款用于家用和养老机构的助老服务机器人。该服务机器人能实现在养老机构内自主导航、语音识别、信息存储、人员搭载、物品运输、生命体征以及每日定时供药等日常看护工作。利用助老服务机器人能够辅助照顾老年人的起居生活，从而帮助青年人减轻照顾老人的压力，该项目是在人口老龄化的背景下探索与创新。本项目针对人口老龄化问题，利用OpenHarmony技术解决联合国17项可持续发展目标中的多项挑战：3、良好的健康与福祉；4、优质教育；8、体面工作经济增长；11、可持续城市和社区。
##### 【初期架构】
###### 【硬件层面】总体的机械结构包含了两个主动轮和一个从动轮、激光雷达、六自由度机械臂、深度相机和机器人底盘驱动板等，通过底盘固定连接各个部分，从而使其配合工作，达到助老服务机器人的设计要求。
![](image/1.jpg)
###### 【软件层面】架构分为三层。第一层，接收激光数据点云，开发APP，显示建模结果；第二层，即为通信层，根据建模结果，通过串口发送数据，底层接受信号，采取最佳路径，从而到达目的地；第三层，通过Hi3861对助老机器人的底盘运动进行控制，同时兼顾多种必要的传感器。
![](image/2.jpg)
#### 【效果展示】：
##### 【底盘运动】
![](image/3.jpg)
##### 【建图演示】
![](image/4.jpg)
###### 产品开发中......
## 【视频链接】
<https://www.bilibili.com/video/BV13T411A7vj/?vd_source=ecd28648a59df782f744a43c818b94e0>