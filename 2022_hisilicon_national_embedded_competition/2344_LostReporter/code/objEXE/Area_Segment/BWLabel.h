//#ifndef _VAP_BWLABEL_
#pragma once
/********************************************************************
	created:	2010/11/15
	created:	15:11:2010   16:09
	filename: 	BWLabel.C
	author:		Hu Jun
	
	purpose:	Label BW Image Regions, 
				Like the Function 'bwlabel' in MATLAB
*********************************************************************/

typedef struct BW_LINE_TABLE
{
	short xLeft, xRight;  // 线段左右端点x坐标
	short yLine;  // 线段y坐标
}s_bw_Line_Table;

typedef struct BW_BLOCK_TABLE
{	
	int lineTable1, lineTable2;  // 区块所包含线段表边界
	int xLeft, xRight;			// 区块左右x边界
	int yTop, yBottom;			// 区块上下y边界
	int area;					// 区块面积
	int centroid_X,centroid_Y;	// 区域质心坐标
	int mask_id;
	unsigned char edge_percent;
	unsigned char static_percent;
	int area_traced; //标示该目标是已被跟踪，0：没有跟踪，1~99：可能跟踪，100；跟踪。//实际使用时超过50就会被置为100
	int type;//区域类型，如人体、车辆
	int area_rec;//外接矩形面积
}s_bw_Block_Table;



#define CONNECTIVITY8 8
#define CONNECTIVITY4 4

#define MAX_BW_LINE 10000
#define MAX_BW_BLOCK 2000



//void BWLabel_init();

//void BWLabel_end();

/************************************************************************/
/* 
函数：BWLabel
功能：实现二值图像的连通区域标记
参数：
	pnBW---指向二值图像数据，要求像素值为0、1
	pnBWLabel---指向标记后的图像
	nImageWidth、nImageHeight---图像高宽
	nMode---邻域的连接模式：8为八连通，4为四连通

返回：图像中区域数目
*/


/************************************************************************/
int BWLabel( unsigned char *pnBW, int nImageWidth, int nImageHeight, int nMode, s_bw_Line_Table *p_line_table, s_bw_Block_Table *p_block_table);

//#define _VAP_BWLABEL_
//#endif
