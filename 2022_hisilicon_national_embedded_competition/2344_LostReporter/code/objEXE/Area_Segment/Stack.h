//#ifndef STACK_H
//#define STACK_H
#pragma once


/*--------函数结果状态-----------*/
#define TRUE 1
#define FALSE 0
#define OK 1
#define ERROR 0
#define OVERFLOW -1
typedef int STATUS;



//首先确定堆栈元素的类型
//堆栈所存元素类型
#define ELEMENT_TYPE int*


//堆栈存储空间初始分配量
#define STACK_INITIAL_SIZE 100
//存储空间分配增量
#define STACK_INCREMENT 20

typedef struct tagSTACK 
{
	ELEMENT_TYPE* pArray; //指向存储元素的数组
	size_t nStackSize;    //当前已分配的存储空间，以元素为单位
	int nTopElement;      //指向栈顶元素的下标，当堆栈为空时，值为-1
}STACK, *PSTACK;




/*--------------------栈基本操作的函数声明-------------------*/

//构造一个空栈
STATUS CreateStack( PSTACK pStack );

//销毁堆栈
STATUS DestroyStack( PSTACK pStack );

//清空堆栈
STATUS ClearStack( PSTACK pStack );

//判断堆栈是否为空
//若空，返回TRUE；不空，返回FALSE
STATUS IsStackEmpty( STACK Stack );

//堆栈元素个数
int StackLength( STACK Stack );

//若栈不为空，则用e返回栈的栈顶元素，并返回OK
//否则，返回ERROR
STATUS GetTopElement( STACK Stack, ELEMENT_TYPE *e );

//压栈
STATUS Push( PSTACK pStack, ELEMENT_TYPE e );

//出栈
//栈不空，出栈，返回OK；否则，返回ERROR
STATUS Pop( PSTACK pStack, ELEMENT_TYPE *e );

//#endif