
#define OWNER_SEARCH_ROI 150
//#define GMM_LEARN_FRAME  500 //系统初始化，多少张frame学习背景
#define MAX_SFG  100000 //遗留物最大阈值
#define MIN_SFG  5  //遗留物最小阈值
#define MAX_FG  1000 //前景最大阈值
#define MIN_FG  10 //20  //前景最小阈值
#define BUFFER_LENGTH  900 //暂存前景的buffer
#define GMM_LONG_LEARN_RATE  0.0001 //长背景学习速度
#define GMM_SHORT_LEARN_RATE 0.002  //短背景学习速度
#define INPUT_RESIZE  1  //影响缩小倍数
