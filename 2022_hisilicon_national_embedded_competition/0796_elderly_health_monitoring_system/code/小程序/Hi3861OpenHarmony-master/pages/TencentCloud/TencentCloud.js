const app = getApp()
var myCompare = require('../../utils/Compare.js')
Page({
  data: {
    productId: app.globalData.productId,
    deviceName: app.globalData.deviceName,
    stateReported: {},
    i:0  
  }, 
  
  onLoad: function (options) {
    console.log("index onLoad")
    if (!app.globalData.productId) {
      wx.showToast({
        title: "产品ID不能为空",
        icon: 'none',
        duration: 3000
      })
      return
    } else if (!app.globalData.deviceName) {
      wx.showToast({
        title: "设备名称不能为空",
        icon: 'none',
        duration: 3000
      })
      return
    }
    // this.update()
  },
  do_finish_item() {
    wx.showModal({
      title: '提示',
      content: '确认进行该操作吗?',
    })
  },


  update() { 
      wx.showLoading() 
          wx.cloud.callFunction({
            name: 'iothub-shadow-query',
            data: {
              ProductId: app.globalData.productId,
              DeviceName: app.globalData.deviceName,
              SecretId: app.globalData.secretId,
              SecretKey: app.globalData.secretKey,
            },
            success: res => {
                  wx.showToast({
                   title: '已刷新',
                    icon: 'none',
                    duration: 200//持续的时间
               })
      
                        let deviceData = JSON.parse(res.result.Data)
                        console.log('deviceData====',deviceData,this)
                        this.setData({
                          stateReported: deviceData.payload.state.reported 
                        }) 
                        const a = this.data.stateReported.Distance
                
                       if(this.data.stateReported.SomeoneFallFlag == 1){
                                   wx.showModal({
                                   content:'有人跌倒',
                                   })
                        }else{
                                    wx.showModal({
                                    content:'刷新成功',
                                    })
                                            }
                   
            },
            fail: err => {
              wx.showToast({
                icon: 'none',
                title: 'Subscribe失败，获取云端数据失败',
              })
              console.error('[云函数] [iotexplorer] 调用失败：', err)
            }
          })   
      },
  
 
  switchChange(e) {
    let value = 0
    if (e.detail.value == true) {
      value = 1
    }
    let item = e.currentTarget.dataset.item
    let obj = {
      [`${item}`]: value
    }
    let payload = JSON.stringify(obj)
    JSON.parse
    console.log(payload)
    wx.showLoading()
    wx.cloud.callFunction({
      name: 'iothub-publish',
      data: {
        SecretId: app.globalData.secretId,
        SecretKey: app.globalData.secretKey,
        ProductId: app.globalData.productId,
        DeviceName: app.globalData.deviceName,
        Topic: app.globalData.productId + "/" + app.globalData.deviceName + "/data",
        Payload: payload,
      },
      success: res => {
        wx.showToast({
          icon: 'none',
          title: 'publish完成',
        })
        console.log("res:", res)
      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: 'publish失败，请连接设备',
        })
        console.error('[云函数] [iotexplorer] 调用失败：', err)
      }
    })  
  
  },



  butt(){
    if(stateReported.HeartRate > 120 || stateReported.BloodOxygen < 95){
     wx.showModal({
      title: '提示',
      content: '出现紧急情况，请注意！',
      })
   }
   
  },
          
  
 
  
    


  complete(){
       wx.showToast({
         title: '{stateReported.BloodOxygen}',
         icon: 'none',
         duration: 2000//持续的时间
       }) 
  },




















  onLoad: function (option) {
    var that = this
    // 监听页面加载的生命周期函数
    setInterval(() => {
        this.data.count += 168
        this.setData({
            count: this.data.count
        })
    }, 3600000)
  },
  
  // onLoad: function (options) {
    
  // },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },
    /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },
  
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
})
