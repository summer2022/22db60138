/*
 * @Author: liu Shihao
 * @Date: 2022-06-12 22:19:03
 * @LastEditors: liu Shihao
 * @LastEditTime: 2022-06-13 19:55:52
 * @FilePath: \bearpi-hm_nano\applications\BearPi\BearPi-HM_Nano\sample\robot\include\adc_voltage.h
 * @Description: 
 * Copyright (c) 2022 by ${fzu} email: logic_fzu@outlook.com, All Rights Reserved.
 */
#ifndef __ADC_VOLTAGE_H
#define __ADC_VOLTAGE_H
#include "robot_main.h"


void adc_voltage_init(void);
float voltage_get_data(void);

#endif 
