import netConfig from './fa-netconfig';
import router from '@system.router';
export default {
    NetConfig: netConfig,
    Product: {
        productId: '',
        productName: ''
    },
    ConfigParams: {
        deviceInfo: {},
        sessionId: ''
    },
    onCreate() {
        console.info('AceApplication onCreate');
    },
    onDestroy() {
        console.info('AceApplication onDestroy');
    },
    data:{
        index:"/pages/index/index",
        english_page:"/pages/english/english",
        game_page:"pages/game_page/game_page",
        labyrinth_page:"pages/labyrinth/labyrinth",
        poetry_page:"pages/poetry/poetry",
        mathmatic_page:"pages/mathmatic/mathmatic",
        global_variable:1,
    },

    ToHomePage(){
        this.work_status ='退出';
        router.replace({
            uri: 'pages/index/index',
        });
    },
    ToGamePage(){
        this.work_status ="寓教于乐界面"
        router.replace({
            uri: 'pages/game_page/game_page'
        });
    },
    ToMathmaticPage(){
        this.work_status ='退出';
        router.replace({
            uri: 'pages/mathmatic/mathmatic',
        });
    },
    ToEnglishPage(){
        this.work_status ='退出';
        router.replace({
            uri: 'pages/english/english',
        });
    },
    ToPoetyPage(){
        this.work_status ='退出';
        router.replace({
            uri: 'pages/poetry/poetry',
        });
    },
    ToLabyrinthPage(){
        this.work_status ='退出';
        router.replace({
            uri: 'pages/labyrinth/labyrinth',
        });
    },
};