// ped_naviDlg.h : header file
//
//{{AFX_INCLUDES()
#include "mscomm.h"
#include "ntgraph.h"
//}}AFX_INCLUDES

#if !defined(AFX_PED_NAVIDLG_H__8D84EEF6_040E_4DAE_91C2_9740B2D52353__INCLUDED_)
#define AFX_PED_NAVIDLG_H__8D84EEF6_040E_4DAE_91C2_9740B2D52353__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CPed_naviDlg dialog

class CPed_naviDlg : public CDialog
{
	// Construction
public:
	CPed_naviDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CPed_naviDlg)
	enum { IDD = IDD_PED_NAVI_DIALOG };
	CMSComm	m_comm;
	CNTGraph	m_ntgraph1;
	double	m_testvalue1;
	double	m_atti1;
	double	m_atti2;
	double	m_atti3;
	double	m_posi1;
	double	m_posi2;
	double	m_posi3;
	double	m_velo1;
	double	m_velo2;
	double	m_velo3;
	double	m_accebasis1;
	double	m_accebasis2;
	double	m_accebasis3;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPed_naviDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CPed_naviDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButton1();
	afx_msg void OnOnCommMscomm1();
	afx_msg void OnButton2();
	afx_msg void OnButton3();
	afx_msg void OnButton4();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnButton5();
	afx_msg void OnAccebegin();
	afx_msg void OnAcceend();
	afx_msg void OnAccebegin2();
	afx_msg void OnAcceend2();
	afx_msg void OnAccebegin3();
	afx_msg void OnAcceend3();
	afx_msg void Onguanyu();
	afx_msg void Onopen();
	afx_msg void OnButton8();
	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PED_NAVIDLG_H__8D84EEF6_040E_4DAE_91C2_9740B2D52353__INCLUDED_)
