import router from '@ohos.router';
import deviceManager from '@ohos.distributedHardware.deviceManager';
import featureAbility from '@ohos.ability.featureAbility';
import { DeviceListDialog } from '../utils/componentUtil'
import { toTime } from '../utils/functionUtil'
import { videos } from '../model/videoData'

@Entry
@Component
struct VideoCreateComponent {
  @State videoIndex: number = 0
  @State SpeedIndex: number = 1
  @State src: string = videos[0].src;
  @State previewUri: string = videos[0].previewUri;
  @State title: string = videos[0].title;
  // 当前视频播放速率
  @State currentProgressRate: number = 1;
  @State autoPlays: boolean = false;
  // 是否显示控制栏
  @State controls: number = 0;
  @State fullScreen: boolean = false;
  // 是否正在播放
  @State isPlay: boolean = false;
  // 滑动条进度
  @State inVerticalSetValue: number = 0
  // 当前播放进度
  @State currentTime: number = 0
  // 视频时长
  @State duration: number = 0
  @State currentSpeed: string = '1.0X'
  @State intervalID: number = 0
  startx: number
  starty: number
  endx: number
  endy: number
  // 可信设备列表
  @Provide deviceList: any[] = [];
  // 设备管理对象
  private deviceMag: any = null
  controller: VideoController = new VideoController();
  scroller: Scroller = new Scroller()
  dialogController: CustomDialogController = new CustomDialogController({
    builder: DeviceListDialog({ videoIndex: this.videoIndex }),
    autoCancel: true,
    alignment: DialogAlignment.Bottom
  });

  // 获取设备列表并且打开设备列表弹窗
  getDeviceList() {
    if (this.deviceMag != null) {
      // 获取所有可信设备列表
      this.deviceList = this.deviceMag.getTrustedDeviceListSync();
    }
    else {
      // 创建设备管理实例
      deviceManager.createDeviceManager("com.haoc.myapplication", (err, data) => {
        if (err) {
          console.error("createDeviceManager failed err: " + JSON.stringify(err));
          return;
        }
        console.info('createDeviceManager successful. Data: ' + JSON.stringify(data))
        this.deviceMag = data;
        this.deviceList = this.deviceMag.getTrustedDeviceListSync();
      });
    }
    // 显示设备列表弹窗
    this.dialogController.open()
  }

  // 释放DeviceManager实例
  aboutToDisappear() {
    this.deviceMag.release();
  }

  onPageShow() {
    var context = featureAbility.getContext()
    context.verifyPermission("ohos.permission.DISTRIBUTED_DATASYNC").then((data) => {
      console.info("======================>verifyPermissionCallback====================>");
      console.info("====>data====>" + JSON.stringify(data));
      if (data != 0) {
        context.requestPermissionsFromUser(
          ["ohos.permission.DISTRIBUTED_DATASYNC"],
          1).then((data) => {
          console.info("====>requestdata====>" + JSON.stringify(data));
        });
      }
    })
  }

  onInit() {
    this.SpeedIndex = 1
    this.isPlay = false
    this.currentTime = 0
    this.currentSpeed = '1.0X'
    this.currentProgressRate = 1
    this.inVerticalSetValue = 0
    clearInterval(this.intervalID)
    this.src = videos[this.videoIndex].src
    this.previewUri = videos[this.videoIndex].previewUri
    this.title = videos[this.videoIndex].title
  }

  setTimer() {
    this.intervalID = setInterval(() => {
      this.currentTime++
      this.inVerticalSetValue++
    }, 1 / this.currentProgressRate * 1000)
  }

  build() {
    Stack() {
      Video({
        src: this.src,
        previewUri: this.previewUri,
        currentProgressRate: this.currentProgressRate,
        controller: this.controller
      })
        .width('100%')
        .height('100%')
        .autoPlay(this.autoPlays)
        .controls(false)  // 不显示默认控制栏
        .onStart(() => {
          this.isPlay = true
          this.setTimer()
        })
        .onPause(() => {
          this.isPlay = false
          clearInterval(this.intervalID);
        })
        .onFinish(() => {
          this.isPlay = false
          clearInterval(this.intervalID);
        })
        .onError(() => {
          this.isPlay = false
          clearInterval(this.intervalID);
        })
        .onPrepared((e) => {
          console.error('onPrepared is ' + e.duration);
          this.duration = e.duration
        })
        .onSeeked((e) => {
          console.error('onSeekedis ' + e.time);
          this.currentTime = e.time
        })
        .gesture(
        TapGesture({ count: 1 })
          .onAction(() => {
            if (this.controls) {
              this.controls = 0;
            } else {
              this.controls = 1;
            }
          })
        )
        .gesture(
        TapGesture({ count: 2 })
          .onAction(() => {
            if (this.isPlay) {
              this.isPlay = !this.isPlay
              this.controller.pause();
            } else {
              this.isPlay = !this.isPlay
              this.controller.start();
            }
          })
        )
        .onTouch((event: TouchEvent) => {
          if (event.type === TouchType.Down) {
            this.startx = event.touches[0].screenX
            this.starty = event.touches[0].screenY
          }
          if (event.type === TouchType.Up) {
            this.endx = event.touches[0].screenX
            this.endy = event.touches[0].screenY
            //右滑
            if (this.endx > this.startx && Math.abs(this.endy - this.starty) < 100) {
              this.videoIndex--
              this.onInit()
            }
            //左滑
            else if (this.endx < this.startx && Math.abs(this.endy - this.starty) < 100) {
              this.videoIndex++
              this.onInit()
            }
          }
        })

      // 控制栏
      Column() {
        Row() {
          Image($r("app.media.ic_public_back_white"))
            .width(24)
            .height(24)
            .onClick(() => {
              router.replace({
                url: 'pages/index',
              })
            })
          Text(this.title)
            .fontColor(Color.White)
            .fontSize(20)
            .width('80%')
            .maxLines(1)
          Image($r('app.media.ic_public_wireless_projection_filled'))
            .width(32)
            .height(32)
            .onClick(() => {
              this.getDeviceList()
            })
        }
        .justifyContent(FlexAlign.SpaceBetween)
        .width('100%')
        .align(Alignment.Top)

        Row() {
          Image(this.isPlay ? $r('app.media.ic_public_pause') : $r('app.media.ic_public_play'))
            .width(32)
            .height(32)
            .onClick(() => {
              this.isPlay = !this.isPlay
              if (this.isPlay) {
                this.controller.start()
              } else {
                this.controller.pause()
              }
            })
          // 当前进度
          Text(toTime(this.currentTime))
            .fontColor(Color.White)
            .maxLines(1)
            .textOverflow({ overflow: TextOverflow.None })
          // 进度条
          Slider({
            value: this.inVerticalSetValue,
            min: 0,
            max: this.duration,
            step: 1,
          })
            .width('54%')
            .blockColor(Color.White)
            .trackColor(Color.Gray)
            .selectedColor(Color.White)
            .onChange((value: number, mode: SliderChangeMode) => {
              this.inVerticalSetValue = value
              this.currentTime = value
              this.controller.setCurrentTime(value, SeekMode.Accurate)
              console.info('value:' + value + 'mode:' + mode.toString())
            })
          // 总时长
          Text(toTime(this.duration))
            .fontColor(Color.White)
            .maxLines(1)
            .textOverflow({ overflow: TextOverflow.None })
          // 速率选择框
          Select([{ value: '0.75X' },
                  { value: '1.0X' },
                  { value: '1.25X' },
                  { value: '1.75X' },
                  { value: '2.0X' }])
            .width('19%')
            .selected(this.SpeedIndex)
            .value(this.currentSpeed)
            .font({ size: 14 })
            .fontColor(Color.White)
            .selectedOptionBgColor(Color.Gray)
            .selectedOptionFont({ size: 14 })
            .selectedOptionFontColor(Color.White)
            .optionBgColor(Color.Black)
            .optionFont({ size: 14 })
            .optionFontColor(Color.White)
            .onSelect((index: number, value: string) => {
              this.SpeedIndex = index
              this.currentSpeed = value
              this.currentProgressRate = parseFloat(value.slice(0, value.indexOf('X') + 1))
              clearInterval(this.intervalID)
              if (this.isPlay) {
                this.setTimer()
              }
            })
          // 全屏
          Image($r('app.media.ic_gallery_full_screen'))
            .width(32)
            .height(32)
            .onClick(() => {
              this.fullScreen = !this.fullScreen
              if (this.fullScreen) {
                this.controller.requestFullscreen(true)
              } else {
                this.controller.exitFullscreen()
              }
            })
        }
        .width('100%')
        .align(Alignment.Bottom)
      }
      .padding(10)
      .justifyContent(FlexAlign.SpaceBetween)
      .alignItems(HorizontalAlign.Start)
      .height('100%')
      .width('100%')
      .visibility(this.controls)
    }
  }
}