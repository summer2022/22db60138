## # HarmonyOS ArkUI入门训练营—健康生活实战#--我们的运动健康
### 一、前言
> 经过四天对HarmonyOS最新开发Api声明式开发范式Ets的学习，结合课程中的一些技术细节于相关UI的逻辑布局，本人设计并开发了此运动健康App。有些部分对华为运动健康App做出了一些参考，总的来说是自己第一次对HarmonyOS开发的一次尝试，App大部分以UI为主，也包括一些交互部分。运动健康App，其它添加了家人一栏，在万物互联的今天，不止个人的运动健康要关注，对于自己亲近的家人，更应该得到关注，所以这App叫我们的运动健康。

###　二、效果展示

*  预览器下效果（部分动态效果无法展示）
*  ![](https://gitee.com/ten-fei/contest/raw/master/2022_ArkUI_Bootcamp/2792498102@qq.com+Our_Sport_Health/article_imgs/static.gif)
*  模拟器下效果
*  ![](https://gitee.com/ten-fei/contest/raw/master/2022_ArkUI_Bootcamp/2792498102@qq.com+Our_Sport_Health/article_imgs/dynamic.gif)

### 三、开发流程
#### 3.1 项目结构展示
*  ![](https://gitee.com/ten-fei/contest/raw/master/2022_ArkUI_Bootcamp/2792498102@qq.com+Our_Sport_Health/article_imgs/view.png)

#### 3.2 开发环境


| 系统  | 开发环境                  | API版本          | 开发方式     |
| ----- | ------------------------- | ---------------- | ------------ |
| Win10 | DevEco Studio 3.0 Release | HarmonyOS API8.0 | 声明式UI开发 |
#### 3.3 logo模块
*  logo暂留页展示
*  ![](https://gitee.com/ten-fei/contest/raw/master/2022_ArkUI_Bootcamp/2792498102@qq.com+Our_Sport_Health/article_imgs/logo.png)
*  logo页到登录页间logo的动画变化
*  
```javascript
Image($r('app.media.logo'))
    .width("150")
    .height("200")
    .objectFit(ImageFit.Contain)
    .margin({ top: 180 })
    // 动画跳转
    .sharedTransition("1001",{
    duration:500
})
```

#### 3.4 登录模块
*  登录展示
*  ![](https://gitee.com/ten-fei/contest/raw/master/2022_ArkUI_Bootcamp/2792498102@qq.com+Our_Sport_Health/article_imgs/login.png)
*  登录模块为基本的UI设计（此处为登录事件的路由跳转）
*  
```javascript
// 登录事件
  toLogin() {
        router.push({
            url: 'pages/index'
        })
  }
```

#### 3.5 菜单栏
* 展示
* ![](https://gitee.com/ten-fei/contest/raw/master/2022_ArkUI_Bootcamp/2792498102@qq.com+Our_Sport_Health/article_imgs/menu.png)
* 菜单栏使用到了Tab与TabContent组件
*  
```javascript
// tab默认颜色
@State tabBarDefaultColor: string = '#707070'
// 当前tab栏索引
@State currentIndex: number = 0;
// tab项目属性
@Builder bottomBuilderBar(name: string, image: Resource, index: number{
    Column() {
        Image(image)
            .width(28)
            .aspectRatio(1)
            .fillColor(this.currentIndex ==
                       index ? '#7cbb6a' : this.tabBarDefaultColor)
        Text(name)
            .fontSize(18)
            .fontColor(this.currentIndex ==
                       index ? '#7cbb6a' : this.tabBarDefaultColor)
    }
    .alignItems(HorizontalAlign.Center)
        .width("100%")
}
build() {
    Tabs({ barPosition: BarPosition.End }) {
      TabContent() {
        Sport()
      }
      .tabBar(this.bottomBuilderBar('运动', $r('app.media.sport'), 0))
      TabContent() {
        Health()
      }
      .tabBar(this.bottomBuilderBar('健康', $r('app.media.health'), 1))
      TabContent() {
        Others()
      }
      .tabBar(this.bottomBuilderBar('他人', $r('app.media.her'), 2))
      TabContent() {
        Me()
      }
      .tabBar(this.bottomBuilderBar('我的', $r('app.media.my'), 3))
    }
    .margin({ bottom: 5 })
    .onChange((index) => {
      this.currentIndex = index;
        })
  }
```

#### 3.6 运动模块
* 运动模块效果展示
* ![](https://gitee.com/ten-fei/contest/raw/master/2022_ArkUI_Bootcamp/2792498102@qq.com+Our_Sport_Health/article_imgs/sport.png)
* 运动页UI（轮播图展示，环形进度条，Canvas绘制统计图）
*  
```javascript
// 轮播图
Swiper() {
    ForEach(this.bannerList, (item: string) => {
        Image(item)
            .width('100%')
            .height(180)
            .objectFit(ImageFit.Contain)
    })
}
	.cachedCount(2)
    .index(0)
    .autoPlay(true)
    .interval(4000)
    .duration(1000)
    .itemSpace(0)
    .curve(Curve.Linear)
    .width("100%")
    .backgroundColor("#fff")
    .padding(10)
    .borderRadius(20)
// 环形进度条
Text('运动步数')
    .fontSize(18)
    .textAlign(TextAlign.Start)
    .fontColor(Color.Gray)
    .width("100%")
Progress({ value: 10, total: 100, type: ProgressType.Ring })
    .color('#67C23A').value(45).width(110)
    .style({ strokeWidth: 15, scaleCount: 30, scaleWidth: 20 })
    .margin({ top: 5 })
Text('步数:6500/步')
    .fontSize(16)
    .textAlign(TextAlign.Center)
    .fontColor(Color.Gray)
    .width("100%")
    .margin({ top: 5 })
// Canvas统计图绘制
Canvas(this.context)
    .width('100%')
    .height(160)
    .onReady(() => {
    if (!this.hasRun) {
        this.context.lineWidth = 1
        this.context.strokeStyle = 'gray'
        for (var i = 0; i < 5; i++) {
            this.context.moveTo(10, this.winHeight - i * 30)
            this.context.lineTo(this.winWidth - 10, this.winHeight - i * 30)
        }
        this.context.stroke()
        this.context.fillStyle = "#9ed900";
        this.context.fillRect(this.winWidth / 14 * 1 - 8, this.winHeight - 140, 16, 140)
        this.context.fillRect(this.winWidth / 14 * 3 - 8, this.winHeight - 120, 16, 120)
        this.context.fillRect(this.winWidth / 14 * 5 - 8, this.winHeight - 100, 16, 100)
        this.context.fillRect(this.winWidth / 14 * 7 - 8, this.winHeight - 125, 16, 125)
        this.context.fillRect(this.winWidth / 14 * 9 - 8, this.winHeight - 90, 16, 90)
        this.context.fillRect(this.winWidth / 14 * 11 - 8, this.winHeight - 115, 16, 115)
        this.context.fillStyle = "#67C23A"
        this.context.fillRect(this.winWidth / 14 * 13 - 8, this.winHeight - 100, 16, 100)
        this.hasRun = true; 
    }
})
```

#### 3.7 健康模块（DevEco预览器不支持WebView下统计图展示）
* 健康模块效果展示
* ![](https://gitee.com/ten-fei/contest/raw/master/2022_ArkUI_Bootcamp/2792498102@qq.com+Our_Sport_Health/article_imgs/health.png)
* 心率和体温统计图用到了WebView组件，WebView组件解析Html网页，在页面中展示
* 而Html网页线上统计图部分又用到的开源图标库Echarts
* 以下为统计图模块核心代码展示
* (1): ets下WebView组件
* 
```javascript
Web({ src: $rawfile('health/heart.html'), controller: new WebController() })
              .width("100%")
              .height(400)
```
* (2): 以下为Html图表代码
*  
```html
<!DOCTYPE html>
<html style="height: 100%">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta charset="utf-8">
    <script src="./echarts.min.js"></script>
</head>
<body style="height: 100%; margin: 0">
    <div id="container1" style="height: 200px; width: 100%;"></div>
    <HR align=center width=100% color=#67C23A SIZE=2>
    <div id="container2" style="height: 200px; width: 100%"></div>
    <script type="text/javascript">
        // 模拟数据
        let base = +new Date(2022, 9, 19);
        let oneDay = 24 * 3600 * 1000;
        let date = [];
        let data1 = [];
        let data2 = [];
        let index = 1
        let now = new Date(base);
        for (let i = 1; i <= 270; i++) {
            if (index % 40 == 0) {
                now = new Date((base += oneDay));
                index = 0
            }
            date.push([now.getMonth(), now.getDate()].join('/'));
            data1.push((Math.random() * 80 + 50 + Math.random() * -10).toFixed(1));
            data2.push((Math.random() * 0.5 + 36.7 + Math.random() * -0.5).toFixed(2));
            index++;
        }
        console.log(date);
        console.log(data1);
        var dom1 = document.getElementById("container1");
        var dom2 = document.getElementById("container2");
        var myChart1 = echarts.init(dom1);
        var myChart2 = echarts.init(dom2);
        option1 = {
            tooltip: {
                trigger: 'axis'
            },
            xAxis: {
                show: false,
                type: 'category',
                boundaryGap: false,
                data: date
            },
            yAxis: {
                type: 'value',
                scale: true,
                min: "40",
                max: "200"
            },
            grid: {
                left: 30,
                right: 10,
                top: 10,
                bottom: 10
            },
            series: [
                {
                    name: 'Fake Data',
                    type: 'line',
                    symbol: 'none',
                    sampling: 'lttb',
                    itemStyle: {
                        color: 'rgb(255, 70, 131)'
                    },
                    areaStyle: {
                        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                            {
                                offset: 0,
                                color: 'rgb(255, 158, 68)'
                            },
                            {
                                offset: 1,
                                color: 'rgb(255, 70, 131)'
                            }
                        ])
                    },
                    data: data1
                }
            ]
        };
        option2 = {
            tooltip: {
                trigger: 'axis'
            },
            xAxis: {
                show: false,
                type: 'category',
                boundaryGap: false,
                data: date
            },
            yAxis: {
                type: 'value',
                scale: true,
                min: "36",
                max: "39"
            },
            grid: {
                left: 30,
                right: 10,
                top: 10,
                bottom: 10
            },
            series: {
                data: data2,
                type: "line",
                smooth: true,
                color: "#67C23A",
                name: "",
                markPoint: {
                    data: [
                        { type: "max", name: "最大值" },
                        { type: "min", name: "最小值" }
                    ]
                }
            }
        };
        myChart1.setOption(option1);
        myChart2.setOption(option2)
    </script>
</body>
</html>
```

#### 3.8 他人模块
* 家人列表效果展示
* ![](https://gitee.com/ten-fei/contest/raw/master/2022_ArkUI_Bootcamp/2792498102@qq.com+Our_Sport_Health/article_imgs/others.png)
* 该模块主要使用到了List组件与字母索引器组件
* (1): List组件代码
*  
```javascript
// 索引
  private index: string[] = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
  'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
  // 家人信息
  private familyInfo: {
        index: string,
        info: {
          name: string,
          icon: string
        }[]
  }[] = [
    { index: 'B', info: [{ name: '爸爸', icon: 'user1' }] },
    { index: 'M', info: [{ name: '妈妈', icon: 'user2' }] },
    { index: 'N', info: [{ name: '奶奶', icon: 'user3' }] },
    { index: 'W', info: [{ name: '外公', icon: 'user4' },
                         { name: '外婆', icon: 'user4' }] },
    { index: 'Y', info: [{ name: '爷爷', icon: 'user3' }] },
  ]
  // List组件
    List() {
      ForEach(this.familyInfo, (item, index) => {
        ListItem() {
          Column() {
            Text(item.index)
              .width("100%")
              .fontSize(18)
              .padding({left:10})
              .textAlign(TextAlign.Start)
            ForEach(item.info, (i, d) => {
              Row() {
                Image('/commons/images/others/' + i.icon + '.png')
                  .objectFit(ImageFit.Contain)
                  .width(30)
                Text(i.name)
                  .fontSize(24)
                  .margin({left:20})
              }
              .width("100%")
              .height(80)
              .padding({left:20})
              .alignItems(VerticalAlign.Center)
              .justifyContent(FlexAlign.Start)
            })
          }
          .padding({ top:10 })
        }
      })
    }
    .width("90%")
```
* (2): 字母索引器AlphabetIndexer组件
*  
```javascript
AlphabetIndexer({ arrayValue: this.index, selected: 1 })
          .selectedColor(0xffffff) // 选中颜色
          .popupColor(0xFFFAF0) // 弹出框颜色
          .selectedBackgroundColor(0xCCCCCC) // 选中背景颜色
          .popupBackground(0xD2B48C) // 弹出框背景颜色
          .usingPopup(true) // 是否显示弹出框
          .selectedFont({ size: 16, weight: FontWeight.Bolder }) // 选中的样式
          .popupFont({ size: 30, weight: FontWeight.Bolder }) // 弹出框的演示
          .itemSize(28) // 每一项的大小正方形
          .alignStyle(IndexerAlign.Left) // 左对齐
          .onSelect((index: number) => {
            console.info(this.index[index] + '被选中了') // 选中的事件
          })
          .margin({ top: 20, bottom: 20 })
          .width("10%")
      }
      .width("100%")
      .backgroundColor("#fff")
      .height("90%")
      .borderRadius(20)
      .alignItems(VerticalAlign.Top)
      .alignItems(VerticalAlign.Top)
```

#### 3.9 个人模块
* 效果展示
* ![](https://gitee.com/ten-fei/contest/raw/master/2022_ArkUI_Bootcamp/2792498102@qq.com+Our_Sport_Health/article_imgs/me.png)
* 该模块为基本组件使用，List组件使用，弹框组件使用，以下展示退出登录UI与逻辑
*  
```javascript
        // 按钮与弹框
        Button('退出登录')
            .width(180)
            .height(45)
            .backgroundColor("#7cbb6a")
            .onClick(() => {
              AlertDialog.show(
                {
                  title: '提示',
                  message: '是否确认退出登录!',
                  primaryButton: {
                    value: '取消',
                    action: () => {
                    }
                  },
                  secondaryButton: {
                    value: '确认',
                    fontColor:Color.Red,
                    action: () => {
                      this.toLogout();
                    }
                  }
                }
              )
            })
        // 退出逻辑
        toLogout() {
          router.push({
          url:'pages/login'
        })
  }
```

### 四、个人总结/问题提出
*  希望华为能尽快更新DevEco Studio，能有支持Api8版本的本地模拟器，这样开发Ets模式下ArkUI更加高效。
*  Ets声明式开发个人感觉与网页开发有很多相似的地方，但开发效率比网页开发更加快速，上手也快。
*  在开发这个App过程中，我发现几个问题：(1): Api8下获取窗口宽度使用的是异步，这样很容易在组件渲染完成后，宽高还没有获取到；(2)：一个page下面只能放置一个Web组件，就是一个Page只能渲染一个WebView不知道是我没找到使用方式，还是原生不支持。
*  在使用华为论坛的MarkDown编辑器的时候遇到一个问题，就是在将代码放入高亮模式为'JavaScript'代码高亮模块时，代码间有空格的话，代码高亮模块会发生失真。