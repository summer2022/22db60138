import router from '@ohos.router';
import curves from '@ohos.curves'

import { MealTimeId } from '../model/EnumModels'
import { Category, FoodInfo } from '../model/TypeModels'
import { getFoodInfo, initDietRecords } from '../model/DataUtil'
import { MealTime } from '../model/FoodData'
import { DietRecord } from '../model/RecordData'

import { ContentTable } from '../components/contentTable'
import { Nutrition } from '../components/nutrition'
import { Calories } from '../components/calories'

@Entry
@Component
struct FoodDetail {
    @State private foodInfo: FoodInfo = router.getParams()["foodId"]
    @State windowHeight: number = 0
    @State imageHeight: number = 357
    @State imageBgColorAlpha: number = 0
    @State animationReverse: boolean = false
    private controller: CustomDialogController = new CustomDialogController({
        builder: Record({foodInfo: this.foodInfo}),
        alignment: DialogAlignment.Bottom
    })

    build() {
        Column() {
            Stack({ alignContent: Alignment.TopStart }) {
                // 食物图片显示
                FoodImageDisplay({ foodItem: this.foodInfo})
                    .height(this.imageHeight)

                // 页页标题
                PageTitle()
            }
            .height(this.imageHeight)
            .backgroundColor(`rgba(255, 255, 255, ${this.imageBgColorAlpha})`)
            // 属性动画
//            .animation({duration: 1000, curve: Curve.EaseInOut})
            .onClick(() => {
                this.animationReverse = !this.animationReverse
                // 显示动画
                animateTo({duration: 1000}, () => {
                    if (this.animationReverse) {
                        this.imageHeight = this.windowHeight
                        this.imageBgColorAlpha = 1
                    } else {
                        this.imageHeight = 357
                        this.imageBgColorAlpha = 0
                    }
                })
            })

            Swiper() {
                // 内容表格
                ContentTable({ foodItemObj: $foodInfo })
                Calories({ foodItemObj: $foodInfo })
                Nutrition({ foodItemObj: $foodInfo })
            }
            .autoPlay(true)
            .indicatorStyle({selectedColor: '#78d05c'})
            .height(300)
            .margin(10)
            .borderRadius(20)
            .backgroundColor(Color.White)

            Button($r('app.string.record_key'))
                .backgroundColor('#78d85c')
                .width('80%')
                .margin({top: 20})
                .onClick(() => {
                    this.controller.open()
                })
        }
        .alignItems(HorizontalAlign.Center)
        .backgroundColor('#FFedf2f5')
        .height('100%')
        .onAreaChange((oldValue: Area, newValue: Area) => {
            this.windowHeight = typeof newValue.height === 'number' ? newValue.height : this.windowHeight
        })
    }
}

@Component
struct FoodImageDisplay {
    private foodItem: FoodInfo

    build() {
        Stack({ alignContent: Alignment.BottomStart }) {
            Image(this.foodItem.image)
                .objectFit(ImageFit.Contain)
                .sharedTransition(this.foodItem.letter, { duration: 1000, curve: curves.cubicBezier(0.2, 0.2, 0.1, 1.0), delay: 100})

            Text(this.foodItem.name)
                .fontSize(26)
                .fontWeight(500)
                .margin({ left: 26, bottom: 17.4 })
        }
        .backgroundColor('#FFedf2f5')
        .height('100%')
    }
}

@Component
struct PageTitle {
    build() {
        Flex({ alignItems: ItemAlign.Start }) {
            Image($r('app.media.back'))
                .width(21.8)
                .height(19.6)
            Text($r('app.string.foodDetail_key'))
                .fontSize(21.8)
                .margin({ left: 17.4 })
        }
        .height(61)
        .backgroundColor('#FFedf2f5')
        .padding({ top: 13, bottom: 15, left: 28.3 })
        .onClick(() => {
            router.back()
        })
    }
}

/**
 * 记录对话框
 */
@CustomDialog
struct Record {
    private foodInfo: FoodInfo
    private controller: CustomDialogController
//    private mileType: string[] = ['早餐', '午餐', '晚餐', '零食']
    private mealTime: Resource = $r('app.strarray.mealTime_labels')
    private foodWeight: string[] = ['25', '50', '100', '150', '200', '250', '300', '350', '400', '450', '500']

//    @State mealTime: string[] = getMileTimes()
    private select: number = 1
    @State mealWeight: number = Number(this.foodWeight[this.select])
    private mealTimeId: MealTimeId = MealTimeId.Lunch

    build() {
        Column() {
            Row({space: 12}) {
                Column() {
                    Text(this.foodInfo.name).fontSize(20).width('100%')
                    Text($r('app.string.kcal_key', this.foodInfo.calories.toString()))
                        .fontSize(17).margin({top: 4})
                        .width('100%')
                }.layoutWeight(1)

                TextPicker({range: this.mealTime, selected: this.select})
                    .layoutWeight(1)
                    .linearGradient({
                        angle: 0,
                        direction: GradientDirection.Top,
                        colors: [[0xfdfdfd, 0.0], [0xe0e0e0, 0.5], [0xfdfdfd, 1]]
                    })
                    .onChange((value: string, index: number) => {
                        console.log('xx 进餐时间：' + value + ' 下标：' + index)
                        this.mealTimeId = index
                    })

                TextPicker({range: this.foodWeight, selected: this.select})
                    .layoutWeight(1)
                    .linearGradient({
                        angle: 0,
                        direction: GradientDirection.Top,
                        colors: [[0xfdfdfd, 0.0], [0xe0e0e0, 0.5], [0xfdfdfd, 1]]
                    })
                    .onChange((value: string, index: number) => {
                        console.log('xx 进餐食物重量：' + value + ' 下标：' + index)
                        this.mealWeight = Number(value)
                    })
            }
            .width('100%')
            .height(128)

            Button($r('app.string.complete_label'), { type: ButtonType.Capsule, stateEffect: true })
                .height(43)
                .width('100%')
                .margin({ top: 33, left: 72, right: 72})
                .backgroundColor('#78d85c')
                .onClick(() => {
                    let dietRecordsList = AppStorage.Get<Array<DietRecord>>('dietRecords')
                    if (dietRecordsList == undefined || dietRecordsList.length == 0) {
                        dietRecordsList = initDietRecords
                    }

                    let dietRecordData = new DietRecord(dietRecordsList.length, this.foodInfo.id, new MealTime(this.mealTimeId), this.mealWeight)

                    dietRecordsList.push(dietRecordData)
                    AppStorage.SetOrCreate<Array<DietRecord>>('dietRecords', dietRecordsList)
                    this.controller.close()
                })
        }
        .cardStyle()
        .height(254)
        .width('90%')
    }
}

@Styles function cardStyle() {
    .backgroundColor(Color.White)
    .margin({left: 24, right: 24})
    .borderRadius(12)
    .padding({ left: 0, top: 20, right: 20, bottom: 20 })
}